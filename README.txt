README.txt

-----------------------

This game 'Golden Fleece' was made during the Hectic Games Jam #2, 2014.

This is the desktop version of the game.

It can be run with, and was made in, Processing 2.2.1 (https://processing.org/download/?processing).

-----------------------

http://www.hanleyweng.com/pages/GoldenFleece/GoldenFleece.html

Team: Xavier Ho, Julian Wilton, Hanley Weng.