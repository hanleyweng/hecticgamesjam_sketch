import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import ddf.minim.AudioPlayer;
import ddf.minim.AudioSample;
import ddf.minim.Minim;

import peasy.PeasyCam;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import processing.core.PVector;
import toxi.geom.Vec3D;
import SimpleOpenNI.SimpleOpenNI;

// This is from Processing2.2.1

@SuppressWarnings("serial")
/**
 * Kinect required.

 Determine projected quad boundaries in 3d space (as captured by the kinect).

 Quad:
 1行2
 |  |
 4行3

 '1,2,3,4' with mouse: determines the corners of the quad.

 'c': resets camera to default position

 'o' and 'p' changes resolution - a resolution of 3 is recommended when calibrating, and resolution of 10 recommended after calibration (to increase efficiency)

 Thresholding Method (#2):
 u U, i I : to set range of HorizontalDistance Quad Targetting
 h H, j J : to set range of VerticalDistance Quad Targetting


 'm' to switch from calibration mode to play mode.

 ----

 Thresholding Method 1 (deprecated):
 't' and mouseX : to set minimum Quad Targetting Threshold (red).
 't' and mouseY : to set maximum Quad Targetting Threshold (green).

 * 
 * @author hanleyweng
 *
 */
public class HecticGameJam2014 extends PApplet {

	SimpleOpenNI kinect;

	PeasyCam cam;
	int res = 10; // pointCloud resolution
	float quadRes = 0.03f; // float between 0 and 1 (representative of %)

	int swidth = 1280;
	int sheight = 800;

	boolean MODE_3DCALLIBRATION = true;

	// MINOR PARAMETRIC VARS
	boolean displayQuadTargettingThreshold = false;

	// HOLDER VARS
	Vec3D[] quadPts = new Vec3D[4];
	float minQuadTargettingThreshold = 0;
	float maxQuadTargettingThreshold = 0;

	float minWidthDiffQuad_Threshold = 20;
	float maxWidthDiffQuad_Threshold = 170;
	float minHeightDiffQuad_Threshold = 40;
	float maxHeightDiffQuad_Threshold = 55;

	boolean allQuadPointsPresent;
	Vec3D closest3DKinectPointToMouse = null;
	// KINECT HOLDER VARS
	int[] depthMap;
	PVector[] realWorldMap;

	// GAME SKETCH VARS
	int fps = 30;
	int window_width = swidth;
	int window_height = sheight;
	int bg_x = -55;
	int bg_y = -80;
	int fg_x = 0;
	int fg_y = 660;
	int fg_tree_x = 65;
	int fg_tree_y = 650;
	int goldbar_x = 200;
	int goldbar_y = 50;
	int ram_x = 175;
	int ram_y = 425;
	int spawn_interval_initial = 240;
	int spawn_interval = spawn_interval_initial;
	float initialLife = 100;
	float life = initialLife;

	int screenshake_frames = 0;
	float screenshakeX = 0;
	float screenshakeY = 0;
	float knockback_radius = 50;
	float treasure_line = 500;
	boolean isRamHit = false;

	boolean is_explosion_queued = false;

	ArrayList<Sprite> trees;
	ArrayList<Sprite> treetop_leaves;
	ArrayList<Sprite> explosions;
	ArrayList<Sprite> craters;
	ArrayList<Sprite> flame_explosions;
	ArrayList<Sprite> bloodsplats;
	ArrayList<Sprite> clouds;
	ArrayList<Sprite> leaves;
	Sprite bush1;
	Sprite bush2;
	Sprite ram;
	Sprite fg_tree;
	ArrayList<Squad> vikings;
	Sprite goldbar_frame;
	Sprite goldbar;
	Sprite goldbar_bg;
	PImage fg;
	PImage bg;

	Minim minim;
	AudioSample explosion_sfx;
	AudioSample flame_explosion_sfx;

	// GAMEMODE VARIABLES
	String GAMEMODE_SCORESCREEN = "GAMEMODE_SCORESCREEN";
	String GAMEMODE_PLAYSCREEN = "GAMEMODE_PLAYSCREEN";

	PlayButton playBtn;
	int prv_stat_pbKills = 0;
	int stat_pbKills = 0;
	int stat_gbKills = 0; // globally most kills
	int stat_gbCombo = 0; // global best combo
	int stat_pbCombo = 0; // personal best combo
	int stat_curCombo = 0;
	PImage gameOverPopup;
	PImage gameOverBase;
	PImage gameOverBest;
	PImage gameOverCombo;
	PImage gameOverGameOver;
	PImage gameOverKills;
	PImage gameOverTapReplay;

	ComboDisplay singleComboDisplay;
	ArrayList<ComboDisplay> comboDisplays = new ArrayList<ComboDisplay>();

	String gameMode = GAMEMODE_SCORESCREEN;
	int screenFrameCount = 0;

	int minComboToDisplay = 2;

	AudioSample game_over_sfx;
	AudioSample playBtn_sfx;
	AudioSample painOw_sfx;
	AudioSample painStolen_sfx;
	AudioPlayer electroHorn_audio;
	AudioPlayer scoreScreen_audio;

	PVector latestHitEvent;

	PFont customFont;
	PFont bebasFont;

	PImage logoImage;

	public void setup() {
		size(swidth, sheight, P3D);

		// SETUP PEASY CAM --------------------------------------------------
		// cam = new PeasyCam(this, swidth * 0.5, sheight * 0.5, -2000, 2665);
		cam = new PeasyCam(this, swidth * 0.5, sheight * 0.5, -2000, 2700);

		// SETUP KINECT -----------------------------------------------------
		kinect = new SimpleOpenNI(this);

		kinect.setMirror(false); // disable mirror

		if (kinect.enableDepth() == false) { // enable depthMap generation
			println("Can't open the depthMap, maybe the camera is not connected!");
			exit();
			return;
		}

		if (kinect.enableRGB() == false) {
			println("Can't open the rgbMap, maybe the camera is not connected or there is no rgbSensor!");
			exit();
			return;
		}

		if (kinect.enableScene() == false) { // enable depthMap generation
			println("Can't open the sceneMap, maybe the camera is not connected!");
			exit();
			return;
		}

		// align depth data to image data
		kinect.alternativeViewPointDepthToImage();

		// SETUP QUAD -----------------------------------------------------
		for (int i = 0; i < quadPts.length; i++) {
			quadPts[i] = new Vec3D();
		}

		// GAME SKETCH SETUP
		frameRate(fps);
		colorMode(HSB, 100);

		minim = new Minim(this);
		explosion_sfx = minim.loadSample("Explosion.mp3", 1024);
		flame_explosion_sfx = minim.loadSample("FlamingExplosion.mp3", 1024);

		trees = new ArrayList<Sprite>();
		treetop_leaves = new ArrayList<Sprite>();
		explosions = new ArrayList<Sprite>();
		flame_explosions = new ArrayList<Sprite>();
		craters = new ArrayList<Sprite>();
		bloodsplats = new ArrayList<Sprite>();
		clouds = new ArrayList<Sprite>();
		leaves = new ArrayList<Sprite>();
		ram = new Sprite("ram", "Sheepy/Sheepy%d.png", 1, 5, 4);
		ram.setPos(new PVector(ram_x, ram_y));
		ram.framecount = 4;
		ram.animate(false);
		fg_tree = new Sprite("fg_tree", "FX/Foreground_Tree/Foreground_Tree%d.png", 1, 7, 4);
		fg_tree.setPos(new PVector(fg_tree_x, fg_tree_y));
		fg_tree.framecount = 7;
		vikings = new ArrayList<Squad>();
		goldbar_frame = new Sprite("goldbar_frame", "HealthBar.png");
		goldbar = new Sprite("goldbar", "Bar.png");
		goldbar_bg = new Sprite("goldbar_bg", "bar_BG.png");
		goldbar_frame.pos = new PVector(goldbar_x, goldbar_y);
		goldbar.pos = new PVector(goldbar_x + 18, goldbar_y + 1); // Hack but works
		goldbar_bg.pos = new PVector(goldbar_x + 18, goldbar_y + 1);
		fg = loadImage("Foreground_Bush.png");
		bg = loadImage("LatestBG.png");

		playBtn = new PlayButton();
		playBtn.location = new PVector(swidth / 2, sheight * 0.85f);
		playBtn.dimensions = new PVector(250, 80);
		gameOverPopup = loadImage("GameOverScreen_Elements/GameOverScreen_AllElements.png");
		gameOverBase = loadImage("GameOverScreen_Elements/gover_base.png");
		gameOverBest = loadImage("GameOverScreen_Elements/gover_best.png");
		gameOverCombo = loadImage("GameOverScreen_Elements/gover_combo.png");
		gameOverGameOver = loadImage("GameOverScreen_Elements/gover_GameOver.png");
		gameOverKills = loadImage("GameOverScreen_Elements/gover_kills.png");
		gameOverTapReplay = loadImage("GameOverScreen_Elements/gover_tapReplay.png");

		gameOverPopup = gameOverBase;

		// Battle arena ingame 2 (theme).mp3
		electroHorn_audio = minim.loadFile("Battle arena ingame 2 (theme).mp3", 1024);// electroHorn.mp3", 1024); //main audio
		electroHorn_audio.rewind();
		electroHorn_audio.loop();
		scoreScreen_audio = minim.loadFile("electroHorn.mp3", 1024);// "scoreScreenMusic.mp3", 1024);
		scoreScreen_audio.setGain(-13);
		scoreScreen_audio.pause();
		game_over_sfx = minim.loadSample("gameOver_Sound.mp3", 1024);
		playBtn_sfx = minim.loadSample("HGJam_PlayBtn_sfx.mp3", 1024);
		painOw_sfx = minim.loadSample("HGJ_PainOw_sfx.mp3", 1024);
		painStolen_sfx = minim.loadSample("HGJ_PainStolen_sfx.mp3", 1024);

		addClouds();
		addBushes();
		addTree(new PVector(800, 420));
		addTree(new PVector(870, 430));
		addTree(new PVector(830, 440));
		addTree(new PVector(360, 435));
		addTree(new PVector(430, 415));
		addTreetopLeaves();

		customFont = loadFont("8BITWONDERNominal-36.vlw");
		bebasFont = loadFont("Bebas-36.vlw");

		logoImage = loadImage("Logo_Shadow.png");

		resetVariablesForNewGame();
	}

	public void resetVariablesForNewGame() {

		// Reset Stats
		stat_pbKills = 0;
		stat_pbCombo = 0;

		// TODO - H may have missed some reset values here - some functions duplicated from void setup
		life = initialLife;
		spawn_interval = spawn_interval_initial;
		vikings = new ArrayList<Squad>();

		ram = new Sprite("ram", "Sheepy/Sheepy%d.png", 1, 5, 4);
		ram.setPos(new PVector(ram_x, ram_y));
		ram.framecount = 4;
		ram.animate(false);
	}

	public void update() {
		// update stat
		calculateCombos();

		if ((frameCount + spawn_interval / 2) % spawn_interval == 0) {
			vikings.add(new Squad(new PVector(width + 50, 780 + random(50)), 2, 200, 10));
			spawn_interval -= 1;
			if (spawn_interval < 30) {
				spawn_interval = 30;
			}
		}
		if ((frameCount - 1) % (int) random(240, 500) == 0) {
			Sprite leaf = new Sprite("leaf", "FX/LeafParticle.png");
			leaf.setPos(new PVector(width + 100, random(100)));
			leaf.angle = random(2 * PI);
			leaves.add(leaf);
		}
	}

	public void updateRegardless() {
		update();

		for (Sprite cloud : clouds) {
			cloud.pos.x -= 0.5f;
			if (cloud.pos.x < -200) {
				cloud.pos.x = width + 200;
			}
		}
	}

	public void drawRegardless() {
		// ////////////////////////////////////////////////////////////
		// SCREENSHAKE
		makeScreenShake();

		// ////////////////////////////////////////////////////////////
		// BACKGROUND
		pushMatrix();
		pushStyle();
		if (is_explosion_queued) {
			tint(3, 25, 100);
			image(bg, bg_x, bg_y);

			explosion_sfx.trigger();
			addScreenShake((int) random(60));
			for (Sprite t : trees) {
				t.rewind();
				t.framecount += (int) random(3);
			}
			for (Sprite t : treetop_leaves) {
				t.rewind();
				t.framecount += (int) random(5);
			}
			bush1.rewind();
			bush2.rewind();
			bush1.framecount += (int) random(2);
			bush2.framecount += (int) random(2);
			fg_tree.rewind();
			fg_tree.framecount += (int) random(2);
			is_explosion_queued = false;
		} else {
			image(bg, bg_x, bg_y);
		}
		popStyle();
		popMatrix();

		// ////////////////////////////////////////////////////////////
		// TREES
		for (int i = trees.size() - 1; i >= 0; i--) {
			Sprite e = trees.get(i);
			e.update();
			e.draw();
		}

		// ////////////////////////////////////////////////////////////
		// TREETOP LEAVES
		for (Sprite s : treetop_leaves) {
			s.update();
			s.draw();
		}

		// ////////////////////////////////////////////////////////////
		// RAM
		pushMatrix();
		pushStyle();
		ram.update();
		if (isRamHit) {
			tint(3, 50, 100);
			isRamHit = false;
		}
		ram.draw();
		popStyle();
		popMatrix();

		// ////////////////////////////////////////////////////////////
		// CLOUDS
		for (Sprite cloud : clouds) {
			pushMatrix();
			scale(0.25f);
			translate(cloud.pos.x * 3, cloud.pos.y * 3);
			cloud.draw();
			popMatrix();
		}
	}

	public void drawScoreScreen() {
		// GRAY OUT BACKGROUND, SLIGHTLY
		pushStyle();
		PImage curScreen = this.get();
		filter(GRAY);
		// tint(100, 54);
		// image(curScreen, 0, 0);
		popStyle();
		// ////////////////////////////////////////////////////////////
		// DRAW SCORESCREEN

		float overallProgress = mapWithCap(screenFrameCount, 0, 90, -0.2f, 1);

		pushMatrix();
		pushStyle();

		float alphaLevel = overallProgress * 100;

		tint(100, alphaLevel * 0.7f);

		translate(swidth / 2, sheight * 0.47f);
		int w = gameOverPopup.width;
		int h = gameOverPopup.height;
		translate(-w / 2, -h / 2);

		// Draw Overlay Image
		float progressLvl = 0;

		progressLvl = mapWithCap(overallProgress, 0, 0.2f, 0, 1);
		tint(100, progressLvl * 100);
		image(gameOverPopup, 0, 0);

		// Text Attributes
		textFont(bebasFont, 36);
		textSize(36);
		textAlign(LEFT, CENTER);
		float barTextOffsetX = 90;
		float barTextOffsetY = 41;

		// DRAW ELEMENTS
		// gameOvertitle
		PImage img;

		pushMatrix();
		scale(0.5f);
		translate(300, -300);
		image(logoImage, 0, 0);

		popMatrix();

		// img = gameOverGameOver;
		// pushMatrix();
		// translate(82, 14);
		// translate(img.width / 2, img.height / 2);
		// progressLvl = mapWithCap(overallProgress, 0.1f, 0.25f, 0, 1);
		// scale(elasticValueAtPctgProgress(progressLvl));
		// translate(-img.width / 2, -img.height / 2);
		// // tint(100, 100);
		// tint(100, progressLvl * 100);
		// image(img, 0, 0);
		// popMatrix();

		// gameOverCombo
		img = gameOverCombo;
		pushMatrix();
		translate(138, 360);
		translate(img.width / 2, img.height / 2);
		progressLvl = mapWithCap(overallProgress, 0.3f, 0.4f, 0, 1);
		scale(elasticValueAtPctgProgress(progressLvl));
		translate(-img.width / 2, -img.height / 2);
		tint(100, progressLvl * 100);
		image(img, 0, 0);
		// text
		fill(100, progressLvl * 100);
		text(stat_pbCombo, barTextOffsetX, barTextOffsetY);
		popMatrix();

		// gameOverBest1
		img = gameOverBest;
		pushMatrix();
		translate(401, 357);
		translate(img.width / 2, img.height / 2);
		progressLvl = mapWithCap(overallProgress, 0.35f, 0.45f, 0, 1);
		scale(elasticValueAtPctgProgress(progressLvl));
		translate(-img.width / 2, -img.height / 2);
		tint(100, progressLvl * 100);
		image(img, 0, 0);
		// text
		fill(100, progressLvl * 100);
		text(stat_gbCombo, barTextOffsetX, barTextOffsetY);
		popMatrix();

		// gameOverKills
		img = gameOverKills;
		pushMatrix();
		translate(138, 459);
		translate(img.width / 2, img.height / 2);
		progressLvl = mapWithCap(overallProgress, 0.4f, 0.5f, 0, 1);
		scale(elasticValueAtPctgProgress(progressLvl));
		translate(-img.width / 2, -img.height / 2);
		tint(100, progressLvl * 100);
		image(img, 0, 0);
		// text
		fill(100, progressLvl * 100);
		text(stat_pbKills, barTextOffsetX, barTextOffsetY);
		popMatrix();

		// gameOverBest2
		img = gameOverBest;
		pushMatrix();
		translate(401, 459);
		translate(img.width / 2, img.height / 2);
		progressLvl = mapWithCap(overallProgress, 0.45f, 0.55f, 0, 1);
		scale(elasticValueAtPctgProgress(progressLvl));
		translate(-img.width / 2, -img.height / 2);
		tint(100, progressLvl * 100);
		image(img, 0, 0);
		// text
		fill(100, progressLvl * 100);
		text(stat_gbKills, barTextOffsetX, barTextOffsetY);
		popMatrix();

		// gameOverTapReplay
		img = gameOverTapReplay;
		pushMatrix();
		translate(139, 579);
		translate(img.width / 2, img.height / 2);
		progressLvl = mapWithCap(overallProgress, 0.5f, 0.7f, 0, 1);
		scale(elasticValueAtPctgProgress(progressLvl));
		translate(-img.width / 2, -img.height / 2);
		tint(100, progressLvl * 100);
		image(img, 0, 0);
		popMatrix();

		popStyle();
		popMatrix();

		// DRAW PLAY BUTTON
		// playBtn.draw(); //playBtn now unused.

		// Also draw playscreenText here for now
		// drawPlayscreenText();
	}

	public void drawPlayScreen() {
		pushMatrix();
		pushStyle();
		noStroke();

		// ////////////////////////////////////////////////////////////
		// CRATERS
		for (int i = craters.size() - 1; i >= 0; i--) {
			Sprite crater = craters.get(i);
			crater.update();
			crater.draw();
			if (crater.isFinished()) {
				craters.remove(i);
			}
		}

		// ////////////////////////////////////////////////////////////
		// DEAD BODIES
		for (int i = vikings.size() - 1; i >= 0; i--) {
			Squad squad = vikings.get(i);
			if (squad.isEmpty()) {
				vikings.remove(i);
				continue;
			}
			squad.update();
			squad.drawOnlyDead();
		}

		// ////////////////////////////////////////////////////////////
		// VIKINGS
		for (int i = vikings.size() - 1; i >= 0; i--) {
			Squad squad = vikings.get(i);
			if (squad.isEmpty()) {
				vikings.remove(i);
				continue;
			}
			squad.drawOnlyAlive();
		}

		// ////////////////////////////////////////////////////////////
		// EXPLOSIONS
		for (int i = explosions.size() - 1; i >= 0; i--) {
			Sprite e = explosions.get(i);
			e.update();
			e.draw();
			if (e.isFinished()) {
				explosions.remove(i);
			}
		}
		for (int i = flame_explosions.size() - 1; i >= 0; i--) {
			Sprite e = flame_explosions.get(i);
			e.update();
			e.draw();
			if (e.isFinished()) {
				flame_explosions.remove(i);
			}
		}

		// ////////////////////////////////////////////////////////////
		// BLOODSPLATS
		for (int i = bloodsplats.size() - 1; i >= 0; i--) {
			Sprite e = bloodsplats.get(i);
			e.update();
			e.draw();
			if (e.isFinished()) {
				bloodsplats.remove(i);
			}
		}

		// ////////////////////////////////////////////////////////////
		// COMBO TEXT DISPLAY
		// if (singleComboDisplay != null) {
		// if (singleComboDisplay.isAlive) {
		// singleComboDisplay.update();
		// singleComboDisplay.draw();
		// }
		// }
		for (int i = 0; i < comboDisplays.size(); i++) {
			ComboDisplay display = comboDisplays.get(i);
			if (display.isAlive) {
				display.update();
				display.draw();
			} else {
				// remove
				comboDisplays.remove(i);
				i--;
			}
		}

		// drawPlayscreenText();

		popStyle();
		popMatrix();
	}

	public void drawRegardlessForeground() {
		// ////////////////////////////////////////////////////////////
		// BUSHES
		pushMatrix();
		scale(1.5f);
		bush1.update();
		bush1.draw();
		bush2.update();
		bush2.draw();
		popMatrix();

		// ////////////////////////////////////////////////////////////
		// FOREGROUND
		pushMatrix();
		pushStyle();
		image(fg, fg_x, fg_y);
		fg_tree.update();
		fg_tree.draw();
		popStyle();
		popMatrix();

		// ////////////////////////////////////////////////////////////
		// ATMOSPHERE PARTICLES
		for (int i = leaves.size() - 1; i >= 0; i--) {
			Sprite e = leaves.get(i);
			e.update();
			e.pos.x -= random(10, 30);
			e.pos.y += random(5, 10);
			e.angle += random(0.001f, 0.1f);
			e.draw();
			if (e.pos.x < -50) {
				leaves.remove(i);
			}
		}
	}

	public void drawGUI() {
		// ////////////////////////////////////////////////////////////
		// HEALTHBAR (IN GOLD!) (Superhackish)
		goldbar_bg.update();
		goldbar.update();
		goldbar_frame.update();
		goldbar_bg.draw();
		PImage img = goldbar.getFrame();
		pushMatrix();
		translate(goldbar.pos.x - 119, goldbar.pos.y - 4);
		scale(life / initialLife * 0.75f, 0.75f);
		image(img, 0, 0);
		popMatrix();
		goldbar_frame.draw();
	}

	public void draw() {
		// Update Kinect
		kinect.update();
		depthMap = kinect.depthMap();
		realWorldMap = kinect.depthMapRealWorld();

		// Update Other Variables
		allQuadPointsPresent = true;
		for (int i = 0; i < 4; i++) {
			if (quadPts.equals(Vec3D.ZERO)) {
				allQuadPointsPresent = false;
				break;
			}
		}

		// ---------------------------------------------------------------------
		// DRAW 3D CALIBRATION SCENE
		if (MODE_3DCALLIBRATION) {
			this.run3dCalibrationMode();
		}

		// ---------------------------------------------------------------------
		// DRAW 2D GAMEMODE

		if (!MODE_3DCALLIBRATION) {
			// /////////////////////////////////////////////////////////

			// GAMEMODE DRAW

			screenFrameCount++;

			updateRegardless();
			drawRegardless();
			if (gameMode.equals(GAMEMODE_PLAYSCREEN)) {
				drawPlayScreen();
			}
			drawRegardlessForeground();
			if (gameMode.equals(GAMEMODE_PLAYSCREEN)) {
				drawGUI();
			}
			if (gameMode.equals(GAMEMODE_SCORESCREEN)) {
				drawScoreScreen();
			}

			// TRANSITION GAME MODES - END GAME
			if (gameMode.equals(GAMEMODE_PLAYSCREEN)) {
				if (life == 0) {
					this.changeGameModeTo(GAMEMODE_SCORESCREEN);
				}
			}

			// /////////////////////////////////////////////////////////

			// quick tests ----
			// rect
			noFill();
			stroke(0, 0, 100);
			strokeWeight(1);
			rect(0, 0, swidth, sheight);
			// mouse
			// fill(0, 0, 100);
			// ellipse(mouseX, mouseY, 20, 20);
			// end quick tests ---

			// ---------------------------------------------------------------------
			// COMPUTE QUAD HITTING PTS

			ArrayList<Vec3D> hitPts_asStagePctg = new ArrayList<Vec3D>();

			// --------------------------------------------
			// Method1 - BruteForce Dots
			// if (allQuadPointsPresent) {
			// int index;
			// PVector realWorldPoint;
			// for (int y = 0; y < kinect.depthHeight(); y += res) {
			// for (int x = 0; x < kinect.depthWidth(); x += res) {
			// index = x + y * kinect.depthWidth();
			// if (depthMap[index] > 0) {
			// // Get Realworld point
			// realWorldPoint = realWorldMap[index];
			//
			// Vec3D realWorldPointVec3d = new Vec3D(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
			//
			// // Get Closest Quad Target Point
			// Vec3D[] closestQuadTargetPt = closestQuadHitTarget_asRealWorldAndPercentage(realWorldPointVec3d);
			// Vec3D closestRealWorldTargetPt = closestQuadTargetPt[0];
			// float dist = closestRealWorldTargetPt.distanceTo(realWorldPointVec3d);
			//
			// // Draw QuadTargetThreshold
			// if (dist < (minQuadTargettingThreshold)) {
			// if (dist > (maxQuadTargettingThreshold)) {
			// // Point is hit!
			// hitPts_asStagePctg.add(closestQuadTargetPt[1]);
			// }
			// }
			// }
			// }
			// }
			// }

			// --------------------------------------------
			// Method2 - Edge Distance
			if (allQuadPointsPresent) {

				// ~~~~~~~~~~~~~~~~~~~
				// THE FOLLOWING UTILISES AVG WIDTH, HEIGHT - HENCE ASSUMES THAT QUAD OPPOSITE EDGES ARE ROUGHLY EQUAL IN LENGTH IN REAL WORLD COORDINATES
				// ~ Get Average Width
				float topLength = quadPts[0].sub(quadPts[1]).magnitude();
				float btmLength = quadPts[3].sub(quadPts[2]).magnitude();
				float avgWidth = (topLength + btmLength) / 2;
				// ~ Get Average Height
				float leftLength = quadPts[1].sub(quadPts[2]).magnitude();
				float rightLength = quadPts[0].sub(quadPts[3]).magnitude();
				float avgHeight = (leftLength + rightLength) / 2;

				int index;
				PVector realWorldPoint;
				for (int y = 0; y < kinect.depthHeight(); y += res) {
					for (int x = 0; x < kinect.depthWidth(); x += res) {
						index = x + y * kinect.depthWidth();
						if (depthMap[index] > 0) {
							// Get Realworld point
							realWorldPoint = realWorldMap[index];

							// Get Edge Distances
							float[] edgeDistances = getDistancesFromQuadEdges(realWorldPoint);

							// Calculate total distances
							float horizontalDistanceSum = edgeDistances[0] + edgeDistances[1];
							float verticalDistanceSum = edgeDistances[2] + edgeDistances[3];

							// Thresholding
							float widthDiff = abs(avgWidth - horizontalDistanceSum);
							float heightDiff = abs(avgHeight - verticalDistanceSum);

							if (widthDiff > minWidthDiffQuad_Threshold) {
								if (widthDiff < maxWidthDiffQuad_Threshold) {
									if (heightDiff > minHeightDiffQuad_Threshold) {
										if (heightDiff < maxHeightDiffQuad_Threshold) {
											// Do one more check - making sure points aren't out of bounds
											if (edgeDistances[0] < avgWidth) {
												if (edgeDistances[1] < avgWidth) {
													if (edgeDistances[2] < avgHeight) {
														if (edgeDistances[3] < avgHeight) {

															// Calculate stagePctg
															float horizontalValue_Pctg = edgeDistances[0] / horizontalDistanceSum;
															float verticalValue_Pctg = edgeDistances[2] / verticalDistanceSum;
															hitPts_asStagePctg.add(new Vec3D(horizontalValue_Pctg, verticalValue_Pctg, 0));

														}
													}
												}
											}

										}
									}
								}
							}

						}
					}
				}
			}

			// ACT ON HITPOINTS
			noStroke();
			fill(20, 80, 90);
			for (Vec3D hitPt : hitPts_asStagePctg) {
				float x = hitPt.x * swidth;
				float y = hitPt.y * sheight;
				// ellipse(x, y, 20, 20);

				// restrict boundary edges
				int boundaryPadding = 50;
				if ((x > boundaryPadding) && (x < swidth - boundaryPadding)) {
					if ((y > boundaryPadding) && (y < sheight - boundaryPadding)) {

						// Fire an event
						this.hitEvent(x, y);
					}
				}

			}

		}

	}

	public void hitEvent(float x, float y) {
		latestHitEvent = new PVector(x, y, 0);

		if (gameMode.equals(this.GAMEMODE_SCORESCREEN)) {
			// if (playBtn.hitTest(x, y)) {
			if (screenFrameCount > 90) { // so that you can't instantly press replay
				this.changeGameModeTo(GAMEMODE_PLAYSCREEN);
				explosion_sfx.trigger();
			}
			// }
			return;
		}
		if (gameMode.equals(this.GAMEMODE_PLAYSCREEN)) {
			// if (y < treasure_line - 30) {
			// return;
			// }
			PVector hitPos = new PVector(x, y);
			this.addExplosion(hitPos);
			this.addCrater(hitPos);
			for (int i = vikings.size() - 1; i >= 0; i--) {
				Squad squad = vikings.get(i);
				squad.hit(hitPos, 80);
			}
			return;
		}
	}

	// -----------------------------------------------------
	// -----------------------------------------------------
	// Han's functions
	public void run3dCalibrationMode() {
		// displays 3d scene and allows for callibration
		// DRAW 3D CALIBRATION SCENE ---------------------------------------
		if (MODE_3DCALLIBRATION) {
			pushMatrix();
			pushStyle();
			colorMode(RGB, 255);

			// Initial Transformations
			background(0, 0, 0);
			translate(swidth / 2, sheight / 2, 0);
			rotateX(radians(180));

			// DRAW KINECT -----------------------------------------------------
			PImage rgbImage = kinect.rgbImage();
			int[] pixelColor = new int[3];

			strokeWeight(max(1, res * 0.5f));

			int index;
			PVector realWorldPoint;
			for (int y = 0; y < kinect.depthHeight(); y += res) {
				for (int x = 0; x < kinect.depthWidth(); x += res) {
					index = x + y * kinect.depthWidth();
					if (depthMap[index] > 0) {
						// get the color of the point
						pixelColor[0] = (int) red(rgbImage.pixels[index]);
						pixelColor[1] = (int) green(rgbImage.pixels[index]);
						pixelColor[2] = (int) blue(rgbImage.pixels[index]);
						stroke(pixelColor[0], pixelColor[1], pixelColor[2]);

						// draw the projected point
						realWorldPoint = realWorldMap[index];
						point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
					}
				}
			}

			// -----------------------------------------------------
			// Determine closest-3dPoint-to-mouse
			closest3DKinectPointToMouse = null;
			float minDist = Float.MAX_VALUE;
			for (int y = 0; y < kinect.depthHeight(); y += res) {
				for (int x = 0; x < kinect.depthWidth(); x += res) {
					index = x + y * kinect.depthWidth();
					if (depthMap[index] > 0) {
						// Get Realworld point
						realWorldPoint = realWorldMap[index];
						float sx = screenX(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
						float sy = screenY(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
						float dist = dist(sx, sy, mouseX, mouseY);
						if (dist < minDist) {
							minDist = dist;
							closest3DKinectPointToMouse = new Vec3D(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
						}
					}
				}
			}
			if (closest3DKinectPointToMouse == null) {
				println("No Closest point.");
			}

			// draw the kinect cam
			kinect.drawCamFrustum();

			// DRAW QUAD ------------------------------------------------------
			// draw quad-points
			pushStyle();
			colorMode(HSB, 100);

			noStroke();
			sphereDetail(5);
			for (int i = 0; i < 4; i++) {
				fill(5 + 15 * i, 90, 90, 90);
				Vec3D quadPt = quadPts[i];
				if (!quadPt.equals(Vec3D.ZERO)) {
					pushMatrix();
					translate(quadPt);
					sphere(15);
					popMatrix();
				}
			}
			// draw quad-rect
			stroke(0, 0, 100);
			strokeWeight(1);
			fill(0, 0, 100, 30);
			if (allQuadPointsPresent) {
				beginShape();
				for (int i = 0; i < quadPts.length; i++) {
					Vec3D pt = quadPts[i];
					vertex(pt.x, pt.y, pt.z);
				}
				endShape(CLOSE);
			}
			popStyle();

			// Draw Quad Hit Targets
			degub_drawQuadHitTargets(true);

			// -----------------------------------------------------
			// REVAEAL QUAD TARGET THRESHOLD RANGE - FOR IDENTIFYING ANY HIT POINTS THAT ARE CLOSE TO SURFACE
			// QuadTargetThreshold - a range by which depth points are considered 'hitting' the quad
			pushStyle();
			colorMode(HSB, 100);

			if (allQuadPointsPresent) {
				for (int y = 0; y < kinect.depthHeight(); y += res) {
					for (int x = 0; x < kinect.depthWidth(); x += res) {
						index = x + y * kinect.depthWidth();
						if (depthMap[index] > 0) {
							// Get Realworld point
							realWorldPoint = realWorldMap[index];

							Vec3D realWorldPointVec3d = new Vec3D(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);

							// Get Closest Quad Target Point
							Vec3D[] closestQuadTargetPt = closestQuadHitTarget_asRealWorldAndPercentage(realWorldPointVec3d);
							Vec3D closestRealWorldTargetPt = closestQuadTargetPt[0];
							float dist = closestRealWorldTargetPt.distanceTo(realWorldPointVec3d);

							// Draw QuadTargetThreshold
							if (displayQuadTargettingThreshold) {
								if (dist < (minQuadTargettingThreshold)) {
									if (dist > (maxQuadTargettingThreshold)) {
										stroke(33, 90, 100);
										point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
									} else {
										stroke(0, 90, 100);
										point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
									}
								}
							}
						}
					}
				}

				// Set QuadTargetThreshold
				if (displayQuadTargettingThreshold) {
					minQuadTargettingThreshold = mouseX * 600.0f / swidth;
					maxQuadTargettingThreshold = mouseY * 600.0f / sheight;
				}

			}
			popStyle();

			// --------------------------------------------
			// Method2 - Edge Distance
			if (allQuadPointsPresent) {

				pushStyle();
				colorMode(HSB, 100);

				// ~~~~~~~~~~~~~~~~~~~
				// THE FOLLOWING UTILISES AVG WIDTH, HEIGHT - HENCE ASSUMES THAT QUAD OPPOSITE EDGES ARE ROUGHLY EQUAL IN LENGTH IN REAL WORLD COORDINATES
				// ~ Get Average Width
				float topLength = quadPts[0].sub(quadPts[1]).magnitude();
				float btmLength = quadPts[3].sub(quadPts[2]).magnitude();
				float avgWidth = (topLength + btmLength) / 2;
				// ~ Get Average Height
				float leftLength = quadPts[1].sub(quadPts[2]).magnitude();
				float rightLength = quadPts[0].sub(quadPts[3]).magnitude();
				float avgHeight = (leftLength + rightLength) / 2;

				// int index;
				// PVector realWorldPoint;
				for (int y = 0; y < kinect.depthHeight(); y += res) {
					for (int x = 0; x < kinect.depthWidth(); x += res) {
						index = x + y * kinect.depthWidth();
						if (depthMap[index] > 0) {
							// Get Realworld point
							realWorldPoint = realWorldMap[index];

							// Get Edge Distances
							float[] edgeDistances = getDistancesFromQuadEdges(realWorldPoint);

							// Calculate total distances
							float horizontalDistanceSum = edgeDistances[0] + edgeDistances[1];
							float verticalDistanceSum = edgeDistances[2] + edgeDistances[3];

							// Thresholding
							// ~ angle between two vectors
							// ~ distance sum addition
							float widthDiff = abs(avgWidth - horizontalDistanceSum);
							float heightDiff = abs(avgHeight - verticalDistanceSum);
							if (widthDiff > minWidthDiffQuad_Threshold) {
								if (widthDiff < maxWidthDiffQuad_Threshold) {
									stroke(70, 100, 100);
									strokeWeight(10);
									point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
								}
							}
							if (heightDiff > minHeightDiffQuad_Threshold) {
								if (heightDiff < maxHeightDiffQuad_Threshold) {
									stroke(25, 100, 100);
									strokeWeight(4);
									point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
								}
							}

							if (widthDiff > minWidthDiffQuad_Threshold) {
								if (widthDiff < maxWidthDiffQuad_Threshold) {
									if (heightDiff > minHeightDiffQuad_Threshold) {
										if (heightDiff < maxHeightDiffQuad_Threshold) {
											// Calculate stagePctg
											// float horizontalValue_Pctg = edgeDistances[0] / horizontalDistanceSum;
											// float verticalValue_Pctg = edgeDistances[2] / verticalDistanceSum;
										}
									}
								}
							}

						}
					}
				}

				popStyle();
			}

			popStyle();
			popMatrix();
		}
		// END DRAW 3D CALIBRATION SCENE ---------------------------------------

		// DRAW 2D OVERLAY SCREEN
		// Draw Calibration Attributes
		pushStyle();
		colorMode(HSB, 100);
		String descript = "";
		descript += "minQuadTargettingThreshold: \t" + minQuadTargettingThreshold;
		descript += "\n";
		descript += "maxQuadTargettingThreshold: \t" + maxQuadTargettingThreshold;
		descript += "\n";
		descript += "\n";
		for (int i = 0; i < 4; i++) {
			descript += "quadPt " + i + ": \t" + quadPts[i];
			descript += "\n";
		}
		descript += "\n";
		descript += "FrameRate: \t" + frameRate;
		descript += "\n";
		descript += "--------------------------------------";
		descript += "\n";
		descript += "minWidthDiffQuad_Threshold: \t" + minWidthDiffQuad_Threshold;
		descript += "\n";
		descript += "maxWidthDiffQuad_Threshold: \t" + maxWidthDiffQuad_Threshold;
		descript += "\n";
		descript += "minHeightDiffQuad_Threshold: \t" + minHeightDiffQuad_Threshold;
		descript += "\n";
		descript += "maxHeightDiffQuad_Threshold: \t" + maxHeightDiffQuad_Threshold;
		descript += "\n";

		// println(descript);
		fill(0, 0, 100);
		textSize(12);
		textAlign(LEFT, TOP);
		text(descript, 10, 10);
		popStyle();

	}

	/**
	 * Get the 4 distances from a point and quad edges.
	 * 
	 * @param point
	 *            - the point from which distances to quad edges are measured
	 * @return float[4] distances = {left,right,top,bottom}
	 */
	public float[] getDistancesFromQuadEdges(PVector point) {
		float[] distances = new float[4];

		// Get Quad's points
		PVector q1 = new PVector(quadPts[0].x, quadPts[0].y, quadPts[0].z);
		PVector q2 = new PVector(quadPts[1].x, quadPts[1].y, quadPts[1].z);
		PVector q3 = new PVector(quadPts[2].x, quadPts[2].y, quadPts[2].z);
		PVector q4 = new PVector(quadPts[3].x, quadPts[3].y, quadPts[3].z);

		// Calculate distances from left, right, top, bottom edges
		distances[0] = pointLineDistance3D(q1, q4, point);
		distances[1] = pointLineDistance3D(q2, q3, point);
		distances[2] = pointLineDistance3D(q1, q2, point);
		distances[3] = pointLineDistance3D(q4, q3, point);

		return distances;
	}

	// @formatter:off
	/**
	 * Returns closest point on the quad surface to a particular testPoint.
	 * 
	 * @param testPoint
	 * @return Vec3D[] closestPoint_asRealWorldAndPercentage where
	 * 	closestPoint[0] is the realWorld representation, and
	 *  closestPoint[1] is the Percentage of quad (width,and representation.
	 */
	// @formatter:on
	public Vec3D[] closestQuadHitTarget_asRealWorldAndPercentage(Vec3D testPoint) {
		Vec3D q1 = quadPts[0];
		Vec3D q2 = quadPts[1];
		Vec3D q3 = quadPts[2];
		Vec3D q4 = quadPts[3];

		float x, y, z;
		// for every 'column' (x, horizontal)
		// for every 'row' (y, vertical)

		float minDistance = Float.MAX_VALUE;
		Vec3D closestPoint_asRealWorld = null;
		Vec3D closestPoint_asPercentage = new Vec3D();

		for (float h = 0; h < 1; h += quadRes) {
			// Determine topColumnPosition
			x = map(h, 0, 1, q1.x, q2.x);
			y = map(h, 0, 1, q1.y, q2.y);
			z = map(h, 0, 1, q1.z, q2.z);
			Vec3D topColumnPos = new Vec3D(x, y, z);
			// Determine bottomColumnPosition
			x = map(h, 0, 1, q4.x, q3.x);
			y = map(h, 0, 1, q4.y, q3.y);
			z = map(h, 0, 1, q4.z, q3.z);
			Vec3D btmColumnPos = new Vec3D(x, y, z);

			for (float v = 0; v < 1; v += quadRes) {
				// Determine curPosition
				x = map(v, 0, 1, topColumnPos.x, btmColumnPos.x);
				y = map(v, 0, 1, topColumnPos.y, btmColumnPos.y);
				z = map(v, 0, 1, topColumnPos.z, btmColumnPos.z);
				Vec3D curPos = new Vec3D(x, y, z);

				float distance = curPos.distanceTo(testPoint);
				if (distance < minDistance) {
					minDistance = distance;
					closestPoint_asRealWorld = curPos.copy();
					closestPoint_asPercentage.x = h; // ~
					closestPoint_asPercentage.y = v; // ~
				}
			}
		}

		Vec3D[] closestPoint_asRealWorldAndPercentage = { closestPoint_asRealWorld, closestPoint_asPercentage };

		return closestPoint_asRealWorldAndPercentage;
	}

	public void degub_drawQuadHitTargets(boolean displayIn3DSpace) {

		noStroke();
		fill(0, 0, 80);

		Vec3D q1 = quadPts[0];
		Vec3D q2 = quadPts[1];
		Vec3D q3 = quadPts[2];
		Vec3D q4 = quadPts[3];

		float x, y, z;
		// for every 'column' (x, horizontal)
		// for every 'row' (y, vertical)
		for (float h = 0; h < 1; h += quadRes) {
			// Determine topColumnPosition
			x = map(h, 0, 1, q1.x, q2.x);
			y = map(h, 0, 1, q1.y, q2.y);
			z = map(h, 0, 1, q1.z, q2.z);
			Vec3D topColumnPos = new Vec3D(x, y, z);
			// Determine bottomColumnPosition
			x = map(h, 0, 1, q4.x, q3.x);
			y = map(h, 0, 1, q4.y, q3.y);
			z = map(h, 0, 1, q4.z, q3.z);
			Vec3D btmColumnPos = new Vec3D(x, y, z);

			for (float v = 0; v < 1; v += quadRes) {
				// Determine curPosition
				x = map(v, 0, 1, topColumnPos.x, btmColumnPos.x);
				y = map(v, 0, 1, topColumnPos.y, btmColumnPos.y);
				z = map(v, 0, 1, topColumnPos.z, btmColumnPos.z);
				Vec3D curPos = new Vec3D(x, y, z);

				// Draw curPos
				if (displayIn3DSpace) {

					pushMatrix();
					translate(curPos);
					sphere(5);
					popMatrix();
				} else {
					ellipse(h * swidth, v * sheight, 20, 20);
				}

			}
		}
	}

	public void keyPressed() {
		// QUAD CALIBRATION CONTROL ---------------------------------------
		if (MODE_3DCALLIBRATION) {
			// for numbers 1,2,3,4 - set the quad-boundary position
			if (key == '1')
				quadPts[0] = closest3DKinectPointToMouse.copy();
			if (key == '2')
				quadPts[1] = closest3DKinectPointToMouse.copy();
			if (key == '3')
				quadPts[2] = closest3DKinectPointToMouse.copy();
			if (key == '4')
				quadPts[3] = closest3DKinectPointToMouse.copy();

			if (key == 't') {
				displayQuadTargettingThreshold = !displayQuadTargettingThreshold;
			}

			// Change res'
			if (key == 'o') {
				res--;
				res = max(res, 1);
				println("res: " + res);
			}
			if (key == 'p') {
				res++;
				println("res: " + res);
			}
			if (key == 'k') {
				quadRes -= 0.01f;
				quadRes = max(quadRes, 0.01f);
				println("quadRes: " + quadRes);
			}
			if (key == 'l') {
				quadRes += 0.01f;
				quadRes = min(quadRes, 0.5f);
				println("quadRes: " + quadRes);
			}
			// Change threshold
			if (key == 'u') {
				minWidthDiffQuad_Threshold -= 5;
			}
			if (key == 'U') {
				minWidthDiffQuad_Threshold += 5;
			}
			if (key == 'i') {
				maxWidthDiffQuad_Threshold -= 5;
			}
			if (key == 'I') {
				maxWidthDiffQuad_Threshold += 5;
			}
			if (key == 'h') {
				minHeightDiffQuad_Threshold -= 5;
			}
			if (key == 'H') {
				minHeightDiffQuad_Threshold += 5;
			}
			if (key == 'j') {
				maxHeightDiffQuad_Threshold -= 5;
			}
			if (key == 'J') {
				maxHeightDiffQuad_Threshold += 5;
			}
		}

		// Reset Camera to default position
		if (key == 'c') {
			cam.reset();
		}
		if (key == 'm') {
			MODE_3DCALLIBRATION = !MODE_3DCALLIBRATION;
		}

		// ////////////
		if (key == 'g') {
			if (gameMode.equals(this.GAMEMODE_SCORESCREEN)) {
				this.changeGameModeTo(GAMEMODE_PLAYSCREEN);
				return;
			}
			if (gameMode.equals(this.GAMEMODE_PLAYSCREEN)) {
				this.changeGameModeTo(GAMEMODE_SCORESCREEN);
				return;
			}
		}

	}

	// ADDITIONAL HELPER FUNCTIONS 行行行行行行行行行行行行行行行行行行行行行行行行
	void translate(Vec3D v) {
		translate(v.x, v.y, v.z);
	}

	public float pointLineDistance3D(PVector linePt1, PVector linePt2, PVector point) {
		// Formula from: http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
		PVector a = PVector.sub(point, linePt1);
		PVector b = PVector.sub(point, linePt2);
		PVector c = PVector.sub(linePt2, linePt1);
		PVector crossAB = a.cross(b);

		float distance = crossAB.mag() / c.mag();

		return distance;
	}

	// -----------------------------------------------------
	// -----------------------------------------------------
	// -----------------------------------------------------
	// Xav's functions

	public void mousePressed() {
		this.hitEvent(mouseX, mouseY);
	}

	public void keyReleased() {
		if (key == ESC) {
			exit();
		}
	}

	// -----------------------------------------------------
	// DEFAULT TAB

	public void changeGameModeTo(String newGameMode) {
		gameMode = newGameMode;
		screenFrameCount = 0;

		if (newGameMode.equals(GAMEMODE_SCORESCREEN)) {
			// pause gamePlay music
			electroHorn_audio.pause();
			// play gameScore music
			scoreScreen_audio.rewind();
			scoreScreen_audio.loop();

			// trigger gameOver sound
			game_over_sfx.trigger();

			// Update Stats
			if (stat_pbCombo > stat_gbCombo) {
				stat_gbCombo = stat_pbCombo;
			}
			if (stat_pbKills > stat_gbKills) {
				stat_gbKills = stat_pbKills;
			}
		}
		if (newGameMode.equals(GAMEMODE_PLAYSCREEN)) {
			// pause gameScore music
			scoreScreen_audio.pause();

			// replay gamePlay music
			electroHorn_audio.rewind();
			electroHorn_audio.loop();

			// reset variables
			resetVariablesForNewGame();
		}
	}

	public void drawPlayscreenText() {
		// PRINT TEXT
		pushStyle();
		textFont(customFont, 36);

		String descript = "";
		descript += "FrameRate:   " + frameRate;
		descript += "\n";
		descript += "\n";
		descript += "\n";
		descript += "Kills:   " + stat_pbKills;
		descript += "\n";
		descript += "Combo:   " + stat_curCombo;
		descript += "\n";
		descript += "PB Combo:   " + stat_pbCombo;
		fill(0, 0, 100);
		textSize(18);
		textAlign(LEFT, TOP);
		text(descript, 10, 10);

		popStyle();
	}

	public void calculateCombos() {
		if (latestHitEvent == null)
			return;

		// Update curCombo
		stat_curCombo = stat_pbKills - prv_stat_pbKills;

		// Execute Combo related events
		float x = latestHitEvent.x;
		float y = latestHitEvent.y;
		if (stat_curCombo > stat_pbCombo) {
			stat_pbCombo = stat_curCombo;
		}
		if (stat_curCombo > minComboToDisplay) {
			// if (singleComboDisplay == null) {
			// singleComboDisplay = new ComboDisplay(stat_curCombo);
			// singleComboDisplay.pos = new PVector(x, y, 0);
			// }
			// if (!singleComboDisplay.isAlive) {
			// singleComboDisplay = new ComboDisplay(stat_curCombo);
			// singleComboDisplay.pos = new PVector(x, y, 0);
			// }
			// /////////////// multiple versions
			String a = (stat_curCombo + 0) + "";
			ComboDisplay comboDisplay = new ComboDisplay(a);
			comboDisplay.pos = new PVector(x, y, 0);
			comboDisplays.add(comboDisplay);
			println(stat_curCombo);
		}

		prv_stat_pbKills = stat_pbKills;
	}

	public class PlayButton {
		PVector location;
		PVector dimensions;

		PlayButton() {
			location = new PVector(20, 20);
			dimensions = new PVector(60, 30);
		}

		void draw() {
			// centre mode drawing
			pushMatrix();
			pushStyle();

			translate(location.x, location.y);
			translate(-dimensions.x / 2, -dimensions.y / 2);
			// Draw Rect
			strokeWeight(2);
			fill(30, 80, 40, 30);
			rect(0, 0, dimensions.x, dimensions.y);

			// Draw Text
			textFont(customFont, 36);
			textSize(24);
			fill(0, 0, 100);
			text("Play", 20, 60);

			popStyle();
			popMatrix();
		}

		boolean hitTest(float x, float y) {
			PVector corner1 = new PVector(location.x - dimensions.x / 2, location.y - dimensions.y / 2);
			PVector corner2 = new PVector(location.x + dimensions.x / 2, location.y + dimensions.y / 2);
			if ((x > corner1.x) && (x < corner2.x)) {
				if ((y > corner1.y) && (y < corner2.y)) {

					// if hit, also play its sound
					playBtn_sfx.trigger();

					return true;
				}
			}
			return false;
		}
	}

	public Sprite addExplosion(PVector pos) {
		Sprite e = new Sprite("explosion", "FX/Explosion_Small/Explosion_small%d.png", 1, 27, 4);
		e.setPos(new PVector(pos.x, pos.y));
		explosions.add(e);
		is_explosion_queued = true;
		return e;
	}

	public Sprite addFlameExplosion(PVector pos) {
		Sprite e = new Sprite("flame_explosion", "FX/Explosion_Bomba/ExplosionBombaMan%d.png", 1, 15, 4);
		e.setPos(new PVector(pos.x, pos.y));
		flame_explosions.add(e);
		return e;
	}

	public Sprite addCrater(PVector pos) {
		Sprite e = new Sprite("crater", "FX/ExplosionHit.png");
		e.setPos(new PVector(pos.x, pos.y));
		e.setLife(100);
		craters.add(e);
		return e;
	}

	public Sprite addBloodsplat(PVector pos) {
		Sprite e = new Sprite("bloodsplat", "FX/BloodSplat/BloodSplat%d.png", 1, 9, 4);
		e.setPos(new PVector(pos.x, pos.y));
		bloodsplats.add(e);
		return e;
	}

	public Sprite addTree(PVector pos) {
		Sprite e = new Sprite("tree", "FX/Tree/TreePushed%d.png", 1, 7, 4);
		e.setPos(new PVector(pos.x, pos.y));
		e.framecount = 6;
		trees.add(e);
		return e;
	}

	public void addTreetopLeaves() {
		Sprite t1 = new Sprite("treetop_leaves_1", "FX/Treetop/Treetop1%d.png", 1, 21, 4);
		t1.setPos(new PVector(825, 100));
		t1.framecount = 20;
		treetop_leaves.add(t1);
		Sprite t2 = new Sprite("treetop_leaves_2", "FX/Treetop/treeTop2%d.png", 1, 40, 4);
		t2.setPos(new PVector(775, 30));
		t2.framecount = 39;
		treetop_leaves.add(t2);
		Sprite t3 = new Sprite("treetop_leaves_3", "FX/Treetop/Treetop3%d.png", 1, 10, 4);
		t3.setPos(new PVector(300, 25));
		t3.framecount = 9;
		treetop_leaves.add(t3);
		Sprite t4 = new Sprite("treetop_leaves_2", "FX/Treetop/treeTop4%d.png", 1, 20, 4);
		t4.setPos(new PVector(50, 25));
		t4.framecount = 19;
		treetop_leaves.add(t4);
	}

	public void addClouds() {
		for (int i = 1; i <= 3; i++) {
			Sprite s = new Sprite("cloud" + i, "FX/Cloud" + i + ".png");
			s.setPos(new PVector(random(width), random(50, 150)));
			clouds.add(s);
		}
	}

	public void addBushes() {
		bush1 = new Sprite("bush1", "FX/Bushes/Bush1%d.png", 1, 7, 4);
		bush1.setPos(new PVector(460, 420));
		bush1.framecount = 6;

		bush2 = new Sprite("bush2", "FX/Bushes/Bush2%d.png", 1, 7, 4);
		bush2.setPos(new PVector(330, 420));
		bush2.framecount = 6;
	}

	public void ramDamage(float damage) {
		life -= damage;
		if (life >= 100) {
			ram.framecount = 4;
		} else if (life < 100 && life >= 75) {
			ram.framecount = 3;
		} else if (life >= 50) {
			ram.framecount = 2;
		} else if (life >= 25) {
			ram.framecount = 1;
		} else {
			ram.framecount = 0;
		}
		if (life < 0) {
			life = 0;
		}

		isRamHit = true;

		// Trigger a random pain sfx
		if (random(1.0f) < 0.5f) {
			painOw_sfx.trigger();
		} else {
			painStolen_sfx.trigger();
		}
	}

	public void addScreenShake(int frames) {
		screenshake_frames = frames;
		screenshakeX = random(1) * abs(randomGaussian() * 5 + 5);
		screenshakeY = random(1) * abs(randomGaussian() * 10 + 10);
	}

	public void makeScreenShake() {
		if (screenshake_frames <= 0) {
			return;
		}
		screenshake_frames--;
		screenshakeX *= -0.6;
		screenshakeY *= -0.6;
		translate(screenshakeX, screenshakeY);
	}

	// ----------------------------------------------------
	// COMBO TAB
	public class ComboDisplay {
		// String comboText;
		int lifeSpan = 30;
		int curAge = 0;
		boolean isAlive = true;

		PVector pos;

		float randomHue;

		String comboNumber;

		ComboDisplay(String comboNumber) {
			this.comboNumber = comboNumber;
			// comboText = "COMBO x " + comboNumber;
			pos = new PVector(swidth * 0.5f, sheight * 0.35f);
			randomHue = random(100);
		}

		void update() {
			if (isAlive) {
				curAge++;
				if (curAge > lifeSpan) {
					isAlive = false;
				}
			}
		}

		void draw() {
			float scaleInProgress = map(curAge, 0, lifeSpan * 0.25f, 0, 1);
			scaleInProgress = min(scaleInProgress, 1);
			scaleInProgress = max(scaleInProgress, 0);

			float riseUpProgress = map(curAge, lifeSpan / 2, lifeSpan, 0, 1);
			riseUpProgress = min(riseUpProgress, 1);
			riseUpProgress = max(riseUpProgress, 0);

			float scaleAmount = elasticValueAtPctgProgress(scaleInProgress);
			float alpha = (1 - riseUpProgress) * 100;
			float yoffset = -riseUpProgress * 40;

			pushMatrix();
			pushStyle();
			translate(pos.x, pos.y);
			translate(40 * 2, 0); // offsetX
			translate(0, yoffset);
			scale(scaleAmount);
			textFont(customFont, 36);
			// Draw Text
			String descript1, descript2, descript3;
			fill(randomHue, 90, 90, alpha);

			descript1 = "COMBO   ";
			textSize(40);
			textAlign(RIGHT, CENTER);
			text(descript1, 0, 0);

			descript2 = "X";
			textSize(60);
			textAlign(CENTER, CENTER);
			text(descript2, 0, 0);

			descript3 = "   " + comboNumber;
			textSize(40);
			textAlign(LEFT, CENTER);
			text(descript3, 0, 0);

			// SECOND DRAW
			translate(-5, -5);
			fill(0, 0, 100, alpha);

			descript1 = "COMBO   ";
			textSize(40);
			textAlign(RIGHT, CENTER);
			text(descript1, 0, 0);

			descript2 = "X";
			textSize(60);
			textAlign(CENTER, CENTER);
			text(descript2, 0, 0);

			descript3 = "   " + comboNumber;
			textSize(40);
			textAlign(LEFT, CENTER);
			text(descript3, 0, 0);

			popStyle();
			popMatrix();
		}
	}

	// ----------------
	// HELPER FUNCTIONS
	/**
	 * same as map() function, except values are capped at start2 and stop2
	 * 
	 * @param value
	 * @param start1
	 * @param stop1
	 * @param start2
	 * @param stop2
	 * @return
	 */
	public float mapWithCap(float value, float start1, float stop1, float start2, float stop2) {
		float v = map(value, start1, stop1, start2, stop2);
		float biggerValue = Math.max(start2, stop2);
		float smallerValue = Math.min(start2, stop2);
		v = Math.min(biggerValue, v);
		v = Math.max(smallerValue, v);
		return v;
	}

	public float elasticValueAtPctgProgress(float x) {
		// the original value can be represented as 1 - at x=1, elasticValue=1
		// ~ rough guess of a curve that can simulate elastic transitions - e.g. bounces
		// 1st version
		// float y = (x - 0.8f) * (x - 0.8f) + 0.95f;
		// 2nd version
		float y = Math.abs(x * x - 0.8f) + 0.8f;
		return y;
	}

	// ----------------------------------------------------
	// SPRITE TAB

	HashMap<String, PImage[]> sprite_cache = new HashMap<String, PImage[]>();

	public void addSprite(String assetName, String filename, int start, int end, int padding) {
		PImage[] frames = new PImage[end];
		for (int i = 0; i <= end - start; i++) {
			String name = filename.replace("%d", nf(start + i, padding));
			frames[i] = loadImage(name);
		}
		sprite_cache.put(assetName, frames);
	}

	public void addSprite(String assetName, String filename) {
		PImage[] frames = new PImage[1];
		frames[0] = loadImage(filename);
		sprite_cache.put(assetName, frames);
	}

	public class Sprite {
		PVector pos;
		float angle;
		PVector scale;
		String asset;
		int framecount;
		int speed;
		int start;
		int end;
		int len;
		int life;
		boolean animated;
		boolean looping;

		Sprite(String asset, String filename) {
			if (!sprite_cache.containsKey(asset)) {
				addSprite(asset, filename);
			}
			this.asset = asset;
			this.framecount = 0;
			this.speed = 0;
			this.start = 0;
			this.end = 0;
			this.len = 0;
			this.animated = true;
			this.looping = false;
			this.angle = 0;
			this.life = -1;
			this.scale = new PVector(1, 1);
		}

		Sprite(String asset, String filename, int start, int end, int padding) {
			// Caching sprites so we don't ever load the same images twice
			if (!sprite_cache.containsKey(asset)) {
				addSprite(asset, filename, start, end, padding);
			}
			this.asset = asset;
			this.framecount = 0;
			this.speed = 1;
			this.start = start;
			this.end = end;
			this.len = end - start;
			this.animated = true;
			this.looping = false;
			this.angle = 0;
			this.life = -1;
			this.scale = new PVector(1, 1);
		}

		public void setPos(PVector pos) {
			this.pos = pos;
		}

		public void setLife(int frames) {
			this.life = frames;
		}

		public void flipHorizontal() {
			this.scale.x *= -1;
		}

		public void rewind() {
			this.framecount = 0;
		}

		public void randomFrame() {
			this.framecount = (int) random(this.len);
		}

		public void loopAnimation(boolean looping) {
			this.looping = looping;
		}

		public void animate(boolean animated) {
			this.animated = animated;
		}

		public void update() {
			if (animated) {
				if (looping) {
					this.framecount = (this.framecount + 1) % this.len;
				} else {
					this.framecount = min(this.framecount + this.speed, this.len);
				}
			}
			if (this.life >= 0) {
				this.life--;
			}
		}

		public PVector getSize() {
			PImage frame = this.getFrame();
			return new PVector(frame.width, frame.height);
		}

		public PImage getFrame() {
			if (looping) {
				return sprite_cache.get(this.asset)[this.framecount % this.len];
			} else {
				return sprite_cache.get(this.asset)[this.framecount];
			}
		}

		public void draw() {
			this.draw(this.pos);
		}

		public void draw(PVector pos) {
			PImage frame = this.getFrame();
			int w = frame.width;
			int h = frame.height;
			pushMatrix();
			pushStyle();
			translate(pos.x, pos.y);
			rotate(this.angle);
			scale(this.scale.x * 0.75f, this.scale.y * 0.75f); // Hack to draw smaller pixels
			if (life >= 0) {
				tint(100, 0, 100, min(life, 100));
			}
			image(frame, -w / 2, -h / 2);
			popStyle();
			popMatrix();
		}

		public boolean isFinished() {
			if (this.life >= 0) {
				return this.life == 0;
			} else {
				return this.framecount >= this.end - this.start;
			}
		}
	}

	// ----------------------------------------------------
	// SQUAD TAB
	int miniboss_counter = 0;

	public class Squad {
		ArrayList<Viking> squad;
		float hitbox = 5;
		float speed = min(2 + frameCount * 0.0001f, 10);
		float confidence = 0.5f;
		int scared_threshold = 1;

		Squad(PVector pos, int number, float spreadX, float spreadY) {
			squad = new ArrayList<Viking>();

			for (int i = 0; i < number; i++) {
				float r = random(1);
				Viking viking;

				miniboss_counter++;
				if (miniboss_counter % 16 == 0) {
					viking = new Viking("miniboss");
				} else if (r < 0.33) {
					viking = new Viking("main");
				} else if (r < 0.66) {
					viking = new Viking("alt");
				} else if (r < 0.75) {
					viking = new Viking("bomba");
				} else if (r < 0.90) {
					viking = new Viking("horse");
				} else {
					viking = new Viking("flag");
				}
				PVector displacement = new PVector();
				if (i > 0) {
					displacement.add(new PVector(abs(randomGaussian() * spreadX), randomGaussian() * spreadY));
				}

				PVector _pos = new PVector();
				_pos.add(pos);
				_pos.add(displacement);
				viking.setPos(_pos);
				squad.add(viking);
			}

			Collections.sort(squad, new SpriteDepthComparitor());
		}

		public void hit(PVector target, float radius) {
			int alive = squad.size();
			for (int i = alive - 1; i >= 0; i--) {
				Viking viking = squad.get(i);
				if (viking.pos.dist(target) - hitbox <= radius) {
					//

					alive--;
					viking.hit(target);
				} else if (viking.pos.dist(target) - hitbox <= (radius + knockback_radius)) { // Shockwave
					viking.knockback(target);
				}
			}

			// Vikings get scared in sudden low numbers
			if (alive <= scared_threshold) {
				for (int i = squad.size() - 1; i >= 0; i--) {
					Viking viking = squad.get(i);
					if (viking.state == "run" && (viking.type == "main" || viking.type == "alt")) {
						viking.state = "run_scared";
					}
				}
			}
		}

		public void add(Viking v) {
			this.squad.add(v);
		}

		public Viking get(int i) {
			return this.squad.get(i);
		}

		public void remove(int i) {
			this.squad.remove(i);
		}

		public void update() {
			for (int i = squad.size() - 1; i >= 0; i--) {
				Viking viking = squad.get(i);
				viking.update();
				viking.advance(speed, confidence);
				if ((viking.state == "hit" && viking.isFinished()) || viking.pos.x < -50 || viking.pos.x > (width * 2)) {
					squad.remove(i);
				}
			}
		}

		public void draw() {
			for (Viking viking : squad) {
				viking.draw();
			}
		}

		public void drawOnlyAlive() {
			for (Viking viking : squad) {
				if (viking.state != "hit") {
					viking.draw();
				}
			}
		}

		public void drawOnlyDead() {
			for (Viking viking : squad) {
				if (viking.state == "hit") {
					viking.draw();
				}
			}
		}

		public boolean isEmpty() {
			return squad.size() == 0;
		}
	}

	class SpriteDepthComparitor implements Comparator<Viking> {
		@Override
		public int compare(Viking a, Viking b) {
			if (a.pos.y == b.pos.y) {
				return 0;
			} else {
				return a.pos.y < b.pos.y ? -1 : 1;
			}
		}
	}

	// ----------------------------------------------------
	// VIKING TAB

	public class Viking {
		PVector pos;
		PVector size;
		HashMap<String, Sprite> animations;
		String state;
		String type;
		int damage;
		int life; // Mini boss only
		int max_life = 3; // Mini boss only

		Sprite[] body_parts;
		float[] angular_velocities;
		PVector[] blast_forces;
		float[] ground_level;
		PVector knockback_force;
		float tint_value;

		Viking(String type) {
			animations = new HashMap<String, Sprite>();
			body_parts = new Sprite[8];
			angular_velocities = new float[8];
			blast_forces = new PVector[8];
			ground_level = new float[8];
			for (int i = 0; i < blast_forces.length; i++) {
				blast_forces[i] = new PVector(0, 0);
			}
			knockback_force = new PVector(0, 0);
			this.type = type;

			// Run Animation
			Sprite run_sprite;
			if (type == "main") {
				run_sprite = new Sprite("viking_run", "Vikings/Main_Viking/VikingRun%d.png", 1, 8, 4);
				damage = 20;
			} else if (type == "alt") {
				run_sprite = new Sprite("viking_alt_run", "Vikings/MainAlt_Viking/VikingRun%d.png", 1, 8, 4);
				damage = 20;
			} else if (type == "bomba") {
				run_sprite = new Sprite("bomba_run", "Vikings/Bomba_Viking/VikingBomba_Run%d.png", 1, 8, 4);
				damage = 50;
			} else if (type == "horse") {
				run_sprite = new Sprite("horse_run", "Vikings/Horse_Viking/HorseViking_Run%d.png", 1, 8, 4);
				damage = 10;
			} else if (type == "flag") {
				run_sprite = new Sprite("flag_run", "Vikings/Flag_Viking/VikingFlag_Run%d.png", 1, 8, 4);
				damage = 10;
			} else if (type == "miniboss") {
				run_sprite = new Sprite("miniboss_run", "Vikings/MiniBoss_Viking/RunMiniBoss%d.png", 1, 8, 4);
				damage = 90;
			} else {
				run_sprite = new Sprite("viking_run", "Vikings/Main_Viking/Run/VikingRun%d.png", 1, 8, 4);
				damage = 20;
			}
			run_sprite.randomFrame();
			run_sprite.loopAnimation(true);
			animations.put("run", run_sprite);
			this.state = "run";
			this.size = run_sprite.getSize();

			// Scared Running Animation
			Sprite run_scared_sprite;
			if (type == "main") {
				run_scared_sprite = new Sprite("viking_run_scared", "Vikings/Main_Viking/VikingRunScared%d.png", 1, 8, 4);
			} else if (type == "alt") {
				run_scared_sprite = new Sprite("viking_alt_run_scared", "Vikings/MainAlt_Viking/VikingRunScared%d.png", 1, 8, 4);
			} else {
				run_scared_sprite = new Sprite("viking_run_scared", "Vikings/Main_Viking/VikingRunScared%d.png", 1, 8, 4);
			}
			run_scared_sprite.randomFrame();
			run_scared_sprite.loopAnimation(true);
			animations.put("run_scared", run_scared_sprite);

			// Attack Animation
			Sprite attack_sprite;
			if (type == "main") {
				attack_sprite = new Sprite("viking_attack", "Vikings/Main_Viking/AttackViking%d.png", 1, 10, 4);
			} else if (type == "alt") {
				attack_sprite = new Sprite("viking_alt_attack", "Vikings/MainAlt_Viking/AttackViking%d.png", 1, 10, 4);
			} else if (type == "miniboss") {
				attack_sprite = new Sprite("miniboss_attack", "Vikings/MiniBoss_Viking/Attack_miniBoss%d.png", 1, 9, 4);
			} else {
				attack_sprite = new Sprite("viking_attack", "Vikings/Main_Viking/AttackViking%d.png", 1, 10, 4);
			}
			animations.put("attack", attack_sprite);

			// GOLD! GOLD! GOLD!
			Sprite gold_sprite;
			if (type == "main") {
				gold_sprite = new Sprite("viking_gold", "Vikings/Main_Viking/VikingRun_Coin%d.png", 1, 8, 4);
			} else if (type == "alt") {
				gold_sprite = new Sprite("viking_alt_gold", "Vikings/MainAlt_Viking/VikingRun_Coin%d.png", 1, 8, 4);
			} else if (type == "horse") {
				gold_sprite = new Sprite("viking_horse_gold", "Vikings/Horse_Viking/HorseViking_Run_Coin%d.png", 1, 8, 4);
			} else if (type == "flag") {
				gold_sprite = new Sprite("viking_flag_gold", "Vikings/Flag_Viking/VikingFlag_Run_Coin%d.png", 1, 8, 4);
			} else if (type == "miniboss") {
				gold_sprite = new Sprite("miniboss_gold", "Vikings/MiniBoss_Viking/RunMiniBoss_WithCoin%d.png", 1, 8, 4);
			} else {
				gold_sprite = new Sprite("viking_gold", "Vikings/Main_Viking/VikingRun_Coin%d.png", 1, 8, 4);
			}
			gold_sprite.randomFrame();
			gold_sprite.loopAnimation(true);
			animations.put("gold", gold_sprite);

			// Sprites for gory bits
			for (int i = 1; i <= 8; i++) {
				String name = "viking_bits_%d";
				name = name.replace("%d", nf(i, 4));
				String filename = "Vikings/Bits/ParticleBits%d.png";
				filename = filename.replace("%d", nf(i, 4));
				body_parts[i - 1] = new Sprite(name, filename);
			}
			tint_value = 0;
			if (this.type == "miniboss") {
				this.life = max_life;
			} else {
				this.life = 1;
			}
		}

		public void addAnimation(String state, Sprite animation) {
			animations.put(state, animation);
		}

		public Sprite getAnimation() {
			return animations.get(this.state);
		}

		public Sprite getAnimation(String state) {
			return animations.get(state);
		}

		public void setPos(PVector pos) {
			this.pos = pos;
		}

		public void setState(String state) {
			this.state = state;
		}

		public void advance(float distance, float confidence) {
			if (this.type == "horse") {
				distance *= 2;
			} else if (this.type == "miniboss") {
				distance /= 2;
			}
			float r = random(1);
			if (this.state == "run") {
				if (r <= confidence / 2) {
					this.pos.x -= distance * 2;
				} else if (r <= confidence) {
					this.pos.x -= distance;
				} else {
					this.pos.x -= distance / 2;
				}
			} else if (this.state == "run_scared") {
				distance *= 2;
				if (r <= confidence / 2) {
					this.pos.x -= distance * 2;
				} else if (r <= confidence) {
					this.pos.x -= distance;
				} else {
					this.pos.x -= distance / 2;
				}
			}
			if (this.state == "run" || this.state == "run_scared") {
				// Path to victory
				if (this.pos.x > 1000 && this.pos.y < 750) { // boat line
					this.pos.y++;
				}
				if (this.pos.x > 800 && this.pos.x < 1200 && this.pos.y > 600) { // bush line
					this.pos.y -= distance / 2;
				}
				if (this.pos.x < 500 && this.pos.y > 550) {
					this.pos.y -= random(3) + distance / 2;
				}
				if (r / 2 <= confidence || abs(this.pos.x / width) > this.pos.y / abs(treasure_line - this.pos.y)) {
					if (this.pos.y > treasure_line) {
						this.pos.y -= random(0.5f, 1);
					} else if (this.pos.y < treasure_line) {
						this.pos.y += (distance - 1);
					}
				}
			}
			if (this.state == "gold") {
				if (r <= confidence / 2) {
					this.pos.x += distance * 2;
				} else if (r <= confidence) {
					this.pos.x += distance;
				} else {
					this.pos.x += distance / 2;
				}
				this.pos.y += distance / 5;
			}
		}

		public void update() {
			if (this.state != "hit") {
				for (int i = 0; i < body_parts.length; i++) {
					body_parts[i].pos = new PVector(this.pos.x, this.pos.y);
				}
			}

			Sprite s = this.getAnimation();
			if (this.state == "run" || this.state == "run_scared") {
				this.pos.add(this.knockback_force);
				if (this.pos.y < treasure_line - 10) {
					this.pos.y = treasure_line - 10;
				} else if (this.pos.y > height - 50) {
					this.pos.y = height - 50;
				}
				this.knockback_force.mult(0.9f);
				s.update();

				if (this.pos.dist(new PVector(ram_x, ram_y)) < 100) {
					if (this.type == "main" || this.type == "alt") {
						this.state = "attack";
					} else if (this.type == "flag" || this.type == "horse") {
						this.state = "gold";
						Sprite new_s = this.getAnimation();
						new_s.flipHorizontal();
						ramDamage(this.damage);
					} else if (this.type == "bomba") {
						flame_explosion_sfx.trigger();
						addFlameExplosion(this.pos);
						hitEvent(this.pos.x, this.pos.y);
						ramDamage(this.damage);
					}
				}
			} else if (this.state == "hit") {
				for (int i = 0; i < body_parts.length; i++) {
					PVector force = blast_forces[i];
					Sprite part = body_parts[i];
					PVector part_pos = new PVector(part.pos.x, part.pos.y);
					part_pos.add(force);
					part.angle += angular_velocities[i];
					if (part_pos.y > ground_level[i]) {
						part_pos.y = ground_level[i];
						force.x *= 0.5;
						angular_velocities[i] *= 0.5;
					} else {
						force.y += 1;
						angular_velocities[i] *= 0.9;
					}
					part.setPos(part_pos);
				}

				// Tint sprites
				tint_value++;
			} else if (this.state == "attack") {
				s.update();
				if (s.isFinished()) { // Attack landed
					this.state = "gold";
					Sprite new_s = this.getAnimation();
					new_s.flipHorizontal();
					ramDamage(this.damage);
				}
			} else if (this.state == "gold") {
				s.update();
			}
		}

		public void draw() {
			Sprite s = this.getAnimation();
			if (this.state == "run" || this.state == "run_scared" || this.state == "attack" || this.state == "gold") {
				pushMatrix();
				pushStyle();
				if (this.type == "miniboss") {
					tint(3, (this.max_life - this.life) * 20, 100);
				}
				s.draw(this.pos);
				popStyle();
				popMatrix();
			} else if (this.state == "hit") {
				pushStyle();
				tint(3, 100, max(150 - this.tint_value, 100), min(max(150 - this.tint_value, 0), 100));
				for (Sprite part : body_parts) {
					part.draw();
				}
				popStyle();
			}
		}

		public void hit(PVector target) {
			if (this.state == "hit") {
				return;
			}

			stat_pbKills++;

			life--;
			if (life > 0) {
				return;
			}

			if (this.state == "gold") {
				int d = this.damage;
				if (this.type == "main" || this.type == "alt") {
					d /= 2;
				}
				ramDamage(-d);
			}
			this.state = "hit";
			// Body part forces initialisation
			for (int i = 0; i < blast_forces.length; i++) {
				blast_forces[i] = new PVector(target.x, target.y);
				blast_forces[i].sub(this.pos);
				blast_forces[i].normalize();
				blast_forces[i].x *= random(-20, 20);
				blast_forces[i].y = -random(10, 25);
				angular_velocities[i] = random(-1, 1);
			}
			Sprite splat = addBloodsplat(this.pos);

			if (target.x > this.pos.x) {
				splat.flipHorizontal();
			}
			for (int i = 0; i < ground_level.length; i++) {
				this.ground_level[i] = this.pos.y + this.size.y / 2 + randomGaussian() * 50;
			}
			if (this.type == "bomba") {
				flame_explosion_sfx.trigger();
				addFlameExplosion(this.pos);
				hitEvent(this.pos.x, this.pos.y);
			}
		}

		public void knockback(PVector target) {
			float distance;
			PVector new_force = new PVector(target.x, target.y);
			new_force.sub(this.pos);
			distance = new_force.mag();
			new_force.normalize();
			new_force.mult((knockback_radius - distance) * 0.1f);
			new_force.y *= 0.75;
			this.knockback_force.add(new_force);
		}

		public boolean isFinished() {
			return tint_value >= 150;
		}
	}
}
