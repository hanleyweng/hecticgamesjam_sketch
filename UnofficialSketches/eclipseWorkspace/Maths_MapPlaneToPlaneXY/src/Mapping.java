import processing.core.*; // This is from Processing2.2.1

@SuppressWarnings("serial")
/**
 * This program takes in a right-angled-triangle, with right-angle on top-left corner on PlaneA and maps it to a similar triangle on PlaneB which is the default 2dscreen plane (PlaneXY) (z=0). No rotation around the z is considered here - we assume both triangles were facing in the same direction ~ upright.
 * @author hanleyweng
 *
 */
public class Mapping extends PApplet {

	int swidth = 50;
	int sheight = 50;

	public void setup() {
		size(swidth, sheight);
		colorMode(HSB, 100);

		// INITIALIZE PROJECTION-CALCULATOR
		// PVector A1 = new PVector(0, 0, 0);
		// PVector A2 = new PVector(1, 0, 0);
		// PVector A3 = new PVector(0, -0.707f, 0.707f);

		// Test Values
		// PVector A1 = new PVector(-272.30933f, -126.42935f, 1120.0f);
		// PVector A2 = new PVector(-52.534164f, -94.5615f, 1210.0f);
		// PVector A3 = new PVector(-172.5031f, -196.02626f, 903.0f);
		// // println("As: " + A1 + ", " + A2 + ", " + A3);
		//
		// PVector B1 = new PVector(0, 2, 0);
		// PVector B2 = new PVector(2, 2, 0);
		// PVector B3 = new PVector(0, 0, 0);
		//
		// ProjectionCalculator_RightTriangleTopLeftCorner projectionCalculator = new ProjectionCalculator_RightTriangleTopLeftCorner(A1, A2, A3, B1, B2, B3);

		// CALCULATE PROJECTED POINT - when feeding in A1,A2,A3 - they should line up with B1,B2,B3
		// PVector point = new PVector();
		// point = projectionCalculator.projectedLocationOfPointA(A1);
		// println(point);
		// point = projectionCalculator.projectedLocationOfPointA(A2);
		// println(point);
		// point = projectionCalculator.projectedLocationOfPointA(A3);
		// println(point);

	}

	public void draw() {
		ellipse(swidth / 2, sheight / 2, 50, 50);
	}

	// ///////////////////////////////////////////////////////////////////////////////////
	// PURE MATH HELPERS

	class MathLine3D {
		// Represented as [x y z] = [At+a Bt+b Ct+c], t = 0...100
		// Where A B C is Coeficcient of X Y & Z's 't' (imagine line was being drawn for every t
		// and where a b c is the intercept values of x y z
		PVector coefficientsTvalues;
		PVector interceptValues;

		MathLine3D() {
		}

		void createWithValues(PVector coefficientTvalues, PVector interceptValues) {
			this.coefficientsTvalues = coefficientTvalues;
			this.interceptValues = interceptValues;
		}

		void createWithTwoPoints(PVector point1, PVector point2) {
			// https://answers.yahoo.com/question/index?qid=20100904084237AAcDWY2
			// Vector equation of a line is given to be P + t(Q-P)
			this.interceptValues = point1;
			this.coefficientsTvalues = PVector.sub(point2, point1);
		}

		public String toString() {
			String s = "";
			s += "[x y z] = ";
			s += "t" + this.coefficientsTvalues + " ";
			s += "+ " + this.interceptValues;
			return s;
		}

		// This could also be calculated given any two points.
	}

	class MathPlane {
		// Represented as: ax + by + cz + d = 0
		// a is the Coefficient of x
		// b is the Coefficient of y
		// c is the Coefficient of z
		// d is the intercept value

		// This equation is of a plane with the nonzero normal vector n (a, b, c)
		// through the point (x0, y0, z0)
		PVector coefficientsForXYZ;
		float interceptValue;

		MathPlane() {
		}

		void createWithValues(PVector coefficientsForXYZ, float interceptValue) {
			this.coefficientsForXYZ = coefficientsForXYZ;
			this.interceptValue = interceptValue;
		}

		void createFromThreePoints(PVector point1, PVector point2, PVector point3) {
			// http://www.had2know.com/academics/equation-plane-through-3-points.html
			// First get two vectors by subtracting one point from the others
			PVector vector1 = PVector.sub(point3, point1);
			PVector vector2 = PVector.sub(point2, point1);

			// Find the cross product of the vectors, hence the verctor that is perpendicular to both.
			// This gives us the normal vector of the plane
			PVector crossVector = new PVector();
			PVector.cross(vector1, vector2, crossVector);

			// Find the intercept - given the normal vector, we can now plug in any of the above points to determine the interceptValue.
			float negativeIntercept = PVector.dot(crossVector, point1);

			this.interceptValue = -negativeIntercept;
			this.coefficientsForXYZ = crossVector.get();
		}

		public String toString() {
			String s = "";
			s += coefficientsForXYZ.x + "x + " + coefficientsForXYZ.y + "y + " + coefficientsForXYZ.z + "z + " + interceptValue + " = 0";
			return s;
		}

	}

	class ProjectionCalculator_RightTriangleTopLeftCorner {
		// This ProjectionCalculator is extremely tied in to the current scenario (mapping two right angle triangles to one another, it also assumes no rotation occurs of plane-intersection-rotation.
		// Also - PlaneB on which PlaneA' will sit is also the PlaneXY (z=0)

		// This was written as an object in order to minimize calculations

		// MAP PLANEA TO PLANEB
		// The following is an incomplete implementation of this method, however it suffices for our purposes.
		// MORE SPECIFICALLY, MAP A PLANE TO PLANE XY

		float[][] rotationToPlaneBMatrix;
		float[][] rotationToAlignmentMatrix;
		float scaleWidth, scaleHeight;

		PVector A1_afterRotationToPlaneB;
		PVector B1;

		ProjectionCalculator_RightTriangleTopLeftCorner(PVector A1, PVector A2, PVector A3, PVector B1, PVector B2, PVector B3) {
			this.B1 = B1.get();

			MathPlane planeA = new MathPlane();
			planeA.createFromThreePoints(A1, A2, A3);
			MathPlane planeB = new MathPlane();
			planeB.createFromThreePoints(B1, B2, B3);

			// TASK:
			// - assume TriangleA and TriangleB are of same angles, though not necessarily same orientation or scale.
			// - A1,A2,A3 and B1,B2,B3 are points on each triangle and correspond to one another.
			// - For our purposes, Triangles are right-angle triangles, with Pt1,2 on one axis, and Pt1,3 on another.
			// - i.e. IMPORTANT ASSUMPTION: We assume that both triangles will line up rotationally after the rotationMatrixAroundAxis.
			// - Given a point on TriangleA, determine the corresponding point in TriangleB

			// ROTATE UNTIL PLANEA' AND PLANE B are parallel
			// Determine lineAB intersecting PlaneA and PlaneB
			MathLine3D lineAB = lineIntersectingTwoPlanes(planeA, planeB);
			// println(lineAB.toString());

			// Determine angleAB as the angle between PlaneA and PlaneB
			// This is the same as the angle between their normals
			float angleAB = PVector.angleBetween(planeB.coefficientsForXYZ, planeA.coefficientsForXYZ);
			angleAB *= -1;
			// println(degrees(angleAB));

			// Determine RotationMatrix from PlaneA to PlaneB using 'RotationMatrixFromAxisAndAngle(normalized(lineAB), angleAB)
			PVector normalizedAxis = lineAB.coefficientsTvalues.get();
			normalizedAxis.normalize();
			rotationToPlaneBMatrix = rotationMatrixAroundAxis_forAngle(normalizedAxis, angleAB);

			// Rotate A1cur point (i.e. z axis should be the same since we're rotating to PlaneXY)
			PVector A1cur = A1.get();
			PVector A2cur = A2.get();
			PVector A3cur = A3.get();
			A1cur = multiplyMatrixAndVector(rotationToPlaneBMatrix, A1cur);
			A2cur = multiplyMatrixAndVector(rotationToPlaneBMatrix, A2cur);
			A3cur = multiplyMatrixAndVector(rotationToPlaneBMatrix, A3cur);
			// println(A1cur);
			// println(A2cur);
			// println(A3cur);
			// println("-");

			// Translate Plane A' to match PlaneB
			// Translate points so A1 is at origin ~
			A1_afterRotationToPlaneB = A1cur.get();
			A2cur.sub(A1_afterRotationToPlaneB);
			A3cur.sub(A1_afterRotationToPlaneB);
			A1cur.sub(A1_afterRotationToPlaneB); // subtract self last
			// println(A1cur);
			// println(A2cur);
			// println(A3cur);
			// println("-");

			// - rotate PlaneA' around angle = difference of(angleOfVector A1->A2, angleOfVector B1->B2)
			// Rotate so A2.y = 0
			// Note - for the general case we would rotate around the normal of PlaneB, however since this is simply the z-axis we can rotateZ instead here
			float rotateToAlignmentAngle = A2cur.heading(); // ~ may need to negate // <- here we can assume to B1->B2 has an angle of zero, and A2'sheading determines the angle of the vector from A1->A2 since A1 is at origin
			rotationToAlignmentMatrix = rotationMatrixAroundAxis_forAngle(new PVector(0, 0, 1), rotateToAlignmentAngle);
			A1cur = multiplyMatrixAndVector(rotationToAlignmentMatrix, A1cur); // <- won't do anything since this is origin
			A2cur = multiplyMatrixAndVector(rotationToAlignmentMatrix, A2cur);
			A3cur = multiplyMatrixAndVector(rotationToAlignmentMatrix, A3cur);
			// println(A1cur);
			// println(A2cur);
			// println(A3cur);
			// println("-");

			// Stretch PlaneA' along axis1 (~width) to match PlaneB
			// Stretch PlaneA' along axis2 (~height) to match PlaneB
			// We assume that B's axis are parallel to two sides of the triangle as so:

			// Since we are mapping to the below, where axis are parallel to sides of the triangle, dividing by width and height will be sufficient to stretch mapping
			// Below function utilises this:
			// @formatter:off
			/**
			 * ---------------> X
			 * |  B1 - B2
			 * |  |   /
			 * |  |  /
			 * |  | /
			 * |  B3
			 * |
			 * v
			 * -Y
			 */
			// @formatter:on
			scaleWidth = PVector.dist(B2, B1) / PVector.dist(A2cur, A1cur);
			scaleHeight = PVector.dist(B3, B1) / PVector.dist(A3cur, A1cur);
			A1cur.x *= scaleWidth;
			A1cur.y *= scaleHeight;
			A2cur.x *= scaleWidth;
			A2cur.y *= scaleHeight;
			A3cur.x *= scaleWidth;
			A3cur.y *= scaleHeight;
			// println(A1cur);
			// println(A2cur);
			// println(A3cur);
			// println("-");

			// Translate from A1cur (currently 0,0,0) to PlaneB's B1
			A1cur.add(B1);
			A2cur.add(B1);
			A3cur.add(B1);
			// println(A1cur);
			// println(A2cur);
			// println(A3cur);
			// println("-");
			// ^ NOTE - When printing the above - they should correspond with the points of B1,B2,B3.
		}

		PVector projectedLocationOfPointA(PVector pointA) {
			PVector pointCur = pointA.get();

			// Rotate - to planeB
			pointCur = multiplyMatrixAndVector(rotationToPlaneBMatrix, pointCur);

			// Translate - so A1' is at 0,0,0
			pointCur.sub(A1_afterRotationToPlaneB);

			// Rotate - so angle(A1,A2)=angle(B1,B2), (which equals zero in our case)
			pointCur = multiplyMatrixAndVector(rotationToAlignmentMatrix, pointCur);

			// Stretch - so dist(A1,A2)=dist(B1,B2), (specific to our current implementation using right-angle-triangles-top-left-corner)
			pointCur.x *= scaleWidth;
			pointCur.y *= scaleHeight;

			// Translate - so A1' is at B1
			pointCur.add(B1);

			return pointCur;
		}
	}

	public MathLine3D lineIntersectingPointAndPlanePerpendicularly(PVector point, MathPlane plane) {
		// https://answers.yahoo.com/question/index?qid=20080212215910AAbsAKN
		// ~ not tested beyond the above example
		// Note this is also the line that is the shortest distance between point and plane
		PVector lineCoeffs = plane.coefficientsForXYZ;
		PVector interceptValues = point.get();
		MathLine3D line = new MathLine3D();
		line.createWithValues(lineCoeffs, interceptValues);
		return line;
	}

	public PVector pointWhereLineIntersectsPlane(MathLine3D line, MathPlane plane) {
		// http://www.netcomuk.co.uk/~jenolive/vect18c.html - verified
		// https://answers.yahoo.com/question/index?qid=20081005181048AAXRDdK - verified

		// Identify 't' in line that satisfies the equation by subbing them into the plane
		float t = -(plane.interceptValue + PVector.dot(plane.coefficientsForXYZ, line.interceptValues)) / (PVector.dot(plane.coefficientsForXYZ, line.coefficientsTvalues));
		println(t);

		// Sub t value into line
		float x = line.interceptValues.x + line.coefficientsTvalues.x * t;
		float y = line.interceptValues.y + line.coefficientsTvalues.y * t;
		float z = line.interceptValues.z + line.coefficientsTvalues.z * t;

		return new PVector(x, y, z);
	}

	public MathLine3D lineIntersectingTwoPlanes(MathPlane planeA, MathPlane planeB) {
		// http://www.netcomuk.co.uk/~jenolive/vect18d.html
		// Note - Planes can be intersecting, parallel, or coincident - here we're just assuming they intersect - http://www.vitutor.com/geometry/space/two_planes.html

		// In future: Confirm that planeA and planeB are not parallel before continuing with this function.

		// Get cross of the normals
		PVector normalA = planeA.coefficientsForXYZ;
		PVector normalB = planeB.coefficientsForXYZ;
		PVector crossAB = new PVector();
		PVector.cross(normalA, normalB, crossAB);

		// Assuming that planes will have points for x = 0
		// Determine y and z when x = 0:
		float x = 0;
		float y = 0;
		float z = 0;

		float a = planeA.coefficientsForXYZ.x;
		float b = planeA.coefficientsForXYZ.y;
		float c = planeA.coefficientsForXYZ.z;
		float d = planeA.interceptValue;
		float e = planeB.coefficientsForXYZ.x;
		float f = planeB.coefficientsForXYZ.y;
		float g = planeB.coefficientsForXYZ.z;
		float h = planeB.interceptValue;

		// Determine Intersecting Point
		PVector anIntersectingPoint = new PVector();

		// Pick a point to test for
		if (crossAB.x != 0) {
			x = 0;
			y = (c * h - d * g) / (b * g - f * c);
			z = (b * h - f * d) / (c * f - g * b);
			anIntersectingPoint = new PVector(0, y, z);
		} else if (crossAB.y != 0) {
			y = 0;
			x = (g * d - c * h) / (c * e - g * a);
			z = (h * a - e * d) / (c * e - g * a);
			anIntersectingPoint = new PVector(x, 0, z);
		} else if (crossAB.z != 0) {
			z = 0;
			x = (d * f - b * h) / (b * e - a * f);
			y = (a * h - d * e) / (b * e - a * f);
		} else {
			System.err.println("CrossAB is 0,0,0 !");
		}

		// Create Line
		MathLine3D line = new MathLine3D();
		line.createWithValues(crossAB, anIntersectingPoint);

		return line;
	}

	/**
	 * Rotation matrix from axis and angle http://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
	 * 
	 * @param axis
	 * @param angle
	 * @return
	 */
	public float[][] rotationMatrixAroundAxis_forAngle(PVector axis, float angle) {
		float x = axis.x;
		float y = axis.y;
		float z = axis.z;

		float[][] matrix = new float[3][3];
		matrix[0][0] = cos(angle) + x * x * (1 - cos(angle));
		matrix[0][1] = y * x * (1 - cos(angle)) + z * sin(angle);
		matrix[0][2] = z * x * (1 - cos(angle)) - y * sin(angle);
		matrix[1][0] = x * y * (1 - cos(angle)) - z * sin(angle);
		matrix[1][1] = cos(angle) + y * y * (1 - cos(angle));
		matrix[1][2] = z * y * (1 - cos(angle)) + x * sin(angle);
		matrix[2][0] = x * z * (1 - cos(angle)) + y * sin(angle);
		matrix[2][1] = y * z * (1 - cos(angle)) - x * sin(angle);
		matrix[2][2] = cos(angle) + z * z * (1 - cos(angle));
		return matrix;
	}

	public float[][] rotationMatrix_forRotationX(float angle) {
		float[][] matrix = new float[][] { { 1, 0, 0 }, { 0, cos(angle), sin(angle) }, { 0, -sin(angle), cos(angle) } };
		return matrix;
	}

	public float[][] rotationMatrix_forRotationY(float angle) {
		angle *= -1;
		float[][] matrix = new float[][] { { cos(angle), 0, -sin(angle) }, { 0, 1, 0 }, { sin(angle), 0, cos(angle) } };
		return matrix;
	}

	public PVector multiplyMatrixAndVector(float[][] matrix, PVector vector) {
		// NOTE - currently assuming matrix is 3x3
		PVector newVector = new PVector();
		newVector.x = vector.x * matrix[0][0] + vector.y * matrix[0][1] + vector.z * matrix[0][2];
		newVector.y = vector.x * matrix[1][0] + vector.y * matrix[1][1] + vector.z * matrix[1][2];
		newVector.z = vector.x * matrix[2][0] + vector.y * matrix[2][1] + vector.z * matrix[2][2];
		return newVector;
	}

}
