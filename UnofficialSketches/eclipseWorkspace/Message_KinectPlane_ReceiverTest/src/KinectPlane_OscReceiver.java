import java.util.ArrayList;

import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;
import processing.core.PVector;

// This is from Processing2.2.1

/**
 * This is a test Receiver for Message_KinectPlane.
 * 
 */
@SuppressWarnings("serial")
public class KinectPlane_OscReceiver extends PApplet {

	// COMMUNICATION VARS
	int portToListenToo = 12000;
	OscP5 oscP5;

	int swidth = 400;
	int sheight = 300;

	ArrayList<PVector> pointsToDraw;

	public void setup() {
		size(swidth, sheight);
		colorMode(HSB, 100);

		// Listen for incoming messages
		oscP5 = new OscP5(this, portToListenToo);

		pointsToDraw = new ArrayList<PVector>();
	}

	public void draw() {
		noStroke();
		fill(0, 0, 0, 10);
		rect(0, 0, swidth, sheight);

		fill(0, 0, 100);
		for (int i = 0; i < pointsToDraw.size(); i++) {
			PVector point = pointsToDraw.get(i);
			if (point != null) {
				ellipse(point.x * swidth, point.y * sheight, 5, 5);
			}
		}
		pointsToDraw.clear();

	}

	/* incoming osc message are forwarded to the oscEvent method. */
	public void oscEvent(OscMessage theOscMessage) {

		/* print the address pattern and the typetag of the received OscMessage */
		print("### received an osc message.");
		print(" addrpattern: " + theOscMessage.addrPattern());
		println(" typetag: " + theOscMessage.typetag());

		// Handle Kinect Position message
		if (theOscMessage.addrPattern().equals("/kpos")) {
			// Note - have not tested if any OSC messages are lost / confused between x and y
			float posX = theOscMessage.get(0).floatValue();
			float posY = theOscMessage.get(1).floatValue();

			PVector p = new PVector(posX, posY);
			pointsToDraw.add(p);
		}
	}
}
