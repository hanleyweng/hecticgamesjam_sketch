import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;

// This is from Processing2.2.1

/**
 * This dualTest should run OscSender and OscReceiver simultaneously.
 * 
 * OscSender sends a message.
 * 
 * OscReceiver listens for a message from OscSender.
 * 
 */
@SuppressWarnings("serial")
public class OscReceiver extends PApplet {

	int swidth = 150;
	int sheight = 150;

	int portToListenToo = 12000;

	OscP5 oscP5;

	public void setup() {
		size(swidth, sheight);
		// colorMode(HSB, 100);

		// Listen for incoming messages
		oscP5 = new OscP5(this, portToListenToo);
	}

	public void draw() {
		ellipse(swidth / 2, sheight / 2, 50, 50);
	}

	/* incoming osc message are forwarded to the oscEvent method. */
	public void oscEvent(OscMessage theOscMessage) {
		/* print the address pattern and the typetag of the received OscMessage */
		print("### received an osc message.");
		print(" addrpattern: " + theOscMessage.addrPattern());
		println(" typetag: " + theOscMessage.typetag());
		background(random(255));
	}

}
