import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;

// This is from Processing2.2.1

/**
 * This dualTest should run OscSender and OscReceiver simultaneously.
 * 
 * OscSender broadcasts a message.
 * 
 * OscReceiver listens for a message from OscSender.
 * 
 */
@SuppressWarnings("serial")
public class OscSender extends PApplet {

	int swidth = 200;
	int sheight = 200;

	int targetPort = 12000;
	String targetIpAddress = "127.0.0.1";

	int portToListenToo = 13000; // won't actually be used by this sketch, just required to initialize osc

	OscP5 oscP5;
	NetAddress targetLocation;

	public void setup() {
		size(swidth, sheight);
		// colorMode(HSB, 100);

		// Initialize OSC
		oscP5 = new OscP5(this, portToListenToo);

		// Set location to send too
		targetLocation = new NetAddress(targetIpAddress, targetPort);

	}

	public void draw() {
		ellipse(swidth / 2, sheight / 2, 50, 50);
	}

	public void mousePressed() {
		/* in the following different ways of creating osc messages are shown by example */
		OscMessage myMessage = new OscMessage("/test");

		myMessage.add(123); /* add an int to the osc message */

		/* send the message */
		oscP5.send(myMessage, targetLocation);
	}

}
