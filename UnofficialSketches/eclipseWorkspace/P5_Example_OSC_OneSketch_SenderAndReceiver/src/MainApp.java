import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;
import processing.core.PApplet;

// This is from Processing2.2.1

/**
 * This sketch demonstrates an OSC sender and receiver in one sketch.
 * 
 * @author hanleyweng
 * 
 */
@SuppressWarnings("serial")
public class MainApp extends PApplet {

	int swidth = 800;
	int sheight = 600;

	int portToListenToo = 12000;
	int targetPort = 12000;
	String targetIpAddress = "127.0.0.1";

	OscP5 oscP5;
	NetAddress targetLocation;

	public void setup() {
		size(swidth, sheight);
		// colorMode(HSB, 100);

		// Listen for incoming messages
		oscP5 = new OscP5(this, portToListenToo);

		targetLocation = new NetAddress(targetIpAddress, targetPort);

	}

	public void draw() {
		ellipse(swidth / 2, sheight / 2, 50, 50);
	}

	public void mousePressed() {
		/* in the following different ways of creating osc messages are shown by example */
		OscMessage myMessage = new OscMessage("/test");

		myMessage.add(123); /* add an int to the osc message */

		/* send the message */
		oscP5.send(myMessage, targetLocation);
	}

	/* incoming osc message are forwarded to the oscEvent method. */
	public void oscEvent(OscMessage theOscMessage) {
		/* print the address pattern and the typetag of the received OscMessage */
		print("### received an osc message.");
		print(" addrpattern: " + theOscMessage.addrPattern());
		println(" typetag: " + theOscMessage.typetag());
		background(random(255));
	}

}
