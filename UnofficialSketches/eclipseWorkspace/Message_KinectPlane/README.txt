README.txt

-----------------------

This game 'Golden Fleece' was made during the Hectic Games Jam #2, 2014.

This is the original Kinect installation version of the game that was created at the end of the jam.

This was built in Eclipse, using Processing Libraries (core (from P5 2.2.1), simpleOpenNI, peasycam, minim).


-----------------------

Physical Installation - Setup Instructions:

	Plug in kinect.

	Determine projected quad boundaries in 3d space (as captured by the kinect).

	Quad:
			1––2
			|  |
			4––3

	'1,2,3,4' with mouse: determines the corners of the quad.

	'c': resets camera to default position

	'o' and 'p' changes resolution - a resolution of 3 is recommended when calibrating, and resolution of 10 recommended after calibration (to increase efficiency)

	Thresholding Method (#2):
	u U, i I : to set range of HorizontalDistance Quad Targetting
	h H, j J : to set range of VerticalDistance Quad Targetting


	'm' to switch from calibration mode to play mode.

-----------------------

http://www.hanleyweng.com/pages/GoldenFleece/GoldenFleece.html

Team: Xavier Ho, Julian Wilton, Hanley Weng.
