import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;

import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;
import peasy.PeasyCam;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import toxi.geom.Vec3D;

//2016-06-07 Upgrading to Open Kinect for Processing
import org.openkinect.processing.*;

// This is from Processing2.2.1

@SuppressWarnings("serial")
/**
 * This sketch returns the normalized 2D Coordinates of a surface registered by a Kinect.
 * 
 * The quad plane is represented like so:
 *  Quad:
 * 	1--2
 * 	|  |
 * 	4--3
 * 
 * Instructions:
 * - Setup the quad plane upon which hitpoints will be registered.
 * - - '1,2,3,4' with mouse: determines the corners of the quad.
 * 
 * - Alter the resolution of the kinect camera for more accurate calibration. Decrease resolution to increase efficiency.
 * - - 'o' and 'p' changes resolution - a resolution of 3 is recommended when calibrating, and resolution of 10 recommended after calibration (to increase efficiency)
 * 
 * - The camera view of the kinect can be panned and zoomed with the mouse.
 * - - 'c': resets camera to default position
 * 
 * - Set distance thresholds from which points will be registered.
 * - - u U, i I : to set range of HorizontalDistance Quad Targetting
 * - - h H, j J : to set range of VerticalDistance Quad Targetting
 * 
 * Advanced:
 * - Resolution of the quad plane (hit) pixels can be altered by pressing 'k' and 'l'
 * 
 * 
 * 
 * DevNotes - Future Improvements:
 * - Proper planar mathematical calculation for shortest distant between point and plane
 * - Allow the Kinect to be jostled. Use frame differencing to identify new pixels.
 * 
 * @author hanleyweng
 *
 */
public class Message_KinectPlane extends PApplet {

	// COMMUNICATION VARS
	int targetPort = 12000;
	String targetIpAddress = "127.0.0.1";
	int portToListenToo = 13000; // won't actually be used by this sketch, just required to initialize osc
	OscP5 oscP5;
	NetAddress targetLocation;

	Kinect kinect;

	PeasyCam cam;
	int res = 10; // pointCloud resolution
	float quadRes = 0.03f; // float between 0 and 1 (representative of %) - resolution of plane on which hitPt to quad is calculated

	int swidth = 1280;
	int sheight = 800;
	int scale = 1000; // real world scale?

	// HOLDER VARS
	Vec3D[] quadPts = new Vec3D[4];
	float minQuadTargettingThreshold = 0;
	float maxQuadTargettingThreshold = 0;

	float minWidthDiffQuad_Threshold = 4;
	float distWidthDiffQuad_Threshold = 64;

	float minHeightDiffQuad_Threshold = 30;
	float distHeightDiffQuad_Threshold = 26;

	boolean allQuadPointsPresent;
	Vec3D closest3DKinectPointToMouse = null;

	// KINECT HOLDER VARS
	int[] depthMap;
	FloatBuffer realWorldMap;
	float[] realWorldMapArray;

	// SETUP
	public void setup() {
		size(swidth, sheight, P3D);

		// INSTRUCTIONS
		println("Please read instructions at the top of the java code.");

		// SETUP PEASY CAM --------------------------------------------------
		cam = new PeasyCam(this, swidth * 0.5, sheight * 0.5, -2000, 2700);

		// SETUP KINECT -----------------------------------------------------
		kinect = new Kinect(this);
		kinect.enableMirror(false); // disable mirror
		kinect.initDepth();
		kinect.initVideo();
		realWorldMapArray = new float[kinect.width * kinect.height * 3];

		// SETUP QUAD's Initial Points -----------------------------------------
		for (int i = 0; i < quadPts.length; i++) {
			quadPts[i] = new Vec3D();
		}

		// SETUP OSC
		// Initialize OSC
		oscP5 = new OscP5(this, portToListenToo);
		// Set location to send too
		targetLocation = new NetAddress(targetIpAddress, targetPort);

	}

	public void draw() {
		// Update Kinect
		depthMap = kinect.getRawDepth();
		realWorldMap = kinect.getDephToWorldPositions();
		realWorldMap.get(realWorldMapArray);
		realWorldMap.rewind();
	

		// Update Other Variables
		allQuadPointsPresent = true;
		for (int i = 0; i < 4; i++) {
			if (quadPts.equals(Vec3D.ZERO)) {
				allQuadPointsPresent = false;
				break;
			}
		}

		// ---------------------------------------------------------------------
		// DRAW 3D CALIBRATION SCENE
		this.run3dCalibrationMode();

		this.drawDebugText();

		// ---------------------------------------------------------------------
		// COMPUTE QUAD HITTING PTS
		if (allQuadPointsPresent) {

			ArrayList<Vec3D> hitPts = getHitPts_asStagePctg();

			// ---------------------------------------------------------------------
			// SEND HIT PTS

			// Randomize the order the hitPts are sent - (helps distribute the positions of coordinates in case not all are read by a program, e.g., if it's overloaded).
			Collections.shuffle(hitPts);

			for (Vec3D pt : hitPts) {
				this.sendOscMessageCoordinates(pt.x, pt.y);
			}

		}
	}

	public void sendOscMessageCoordinates(float x, float y) {
		// Send OSC Message
		OscMessage myMessage = new OscMessage("/kpos");
		myMessage.add(x);
		myMessage.add(y);
		/* send the message */
		oscP5.send(myMessage, targetLocation);
	}

	public ArrayList<Vec3D> getHitPts_asStagePctg() {
		if (!allQuadPointsPresent) {
			System.err.println("Not all quad points present.");
			return null;
		}

		ArrayList<Vec3D> hitPts_asStagePctg = new ArrayList<Vec3D>();

		// ~~~~~~~~~~~~~~~~~~~
		// THE FOLLOWING UTILISES AVG WIDTH, HEIGHT - HENCE ASSUMES THAT
		// QUAD OPPOSITE EDGES ARE ROUGHLY EQUAL IN LENGTH IN REAL WORLD
		// COORDINATES
		// ~ Get Average Width
		float topLength = quadPts[0].sub(quadPts[1]).magnitude();
		float btmLength = quadPts[3].sub(quadPts[2]).magnitude();
		float avgWidth = (topLength + btmLength) / 2;
		// ~ Get Average Height
		float leftLength = quadPts[1].sub(quadPts[2]).magnitude();
		float rightLength = quadPts[0].sub(quadPts[3]).magnitude();
		float avgHeight = (leftLength + rightLength) / 2;

		float maxWidthDiffQuad_Threshold = minWidthDiffQuad_Threshold + distWidthDiffQuad_Threshold;
		float maxHeightDiffQuad_Threshold = minHeightDiffQuad_Threshold + distHeightDiffQuad_Threshold;

		int index;
		PVector realWorldPoint;
		for (int y = 0; y < kinect.height; y += res) {
			for (int x = 0; x < kinect.width; x += res) {
				index = x + y * kinect.width;
				if (depthMap[index] > 0) {
					// Get Realworld point
					realWorldPoint = new PVector(realWorldMapArray[index*3], realWorldMapArray[index*3+1], realWorldMapArray[index*3+2]);

					// Get Edge Distances
					float[] edgeDistances = getDistancesFromQuadEdges(realWorldPoint);

					// Calculate total distances
					float horizontalDistanceSum = edgeDistances[0] + edgeDistances[1];
					float verticalDistanceSum = edgeDistances[2] + edgeDistances[3];

					// Thresholding
					float widthDiff = abs(avgWidth - horizontalDistanceSum) * scale;
					float heightDiff = abs(avgHeight - verticalDistanceSum) * scale;	

					if (widthDiff > minWidthDiffQuad_Threshold) {
						if (widthDiff < maxWidthDiffQuad_Threshold) {
							if (heightDiff > minHeightDiffQuad_Threshold) {
								if (heightDiff < maxHeightDiffQuad_Threshold) {
									// Do one more check - making sure
									// points aren't out of bounds
									if (edgeDistances[0] < avgWidth) {
										if (edgeDistances[1] < avgWidth) {
											if (edgeDistances[2] < avgHeight) {
												if (edgeDistances[3] < avgHeight) {

													// Calculate
													// stagePctg
													float horizontalValue_Pctg = edgeDistances[0] / horizontalDistanceSum;
													float verticalValue_Pctg = edgeDistances[2] / verticalDistanceSum;
													hitPts_asStagePctg.add(new Vec3D(horizontalValue_Pctg, verticalValue_Pctg, 0));

												}
											}
										}
									}

								}
							}
						}
					}

				}
			}
		}
		return hitPts_asStagePctg;
	}

	public void run3dCalibrationMode() {
		// displays 3d scene and allows for callibration
		// DRAW 3D CALIBRATION SCENE ---------------------------------------
		pushMatrix();
		pushStyle();
		colorMode(RGB, 255);

		// Initial Transformations
		background(0, 0, 0);
		translate(swidth / 2, sheight / 2, 0);
		scale(150);
//		rotateX(radians(180)); // Upside down mounting

		// DRAW KINECT -----------------------------------------------------
		PImage rgbImage = kinect.getVideoImage();
		int[] pixelColor = new int[3];

		strokeWeight(max(1, res * 0.5f));

		int index;
		PVector realWorldPoint;
		for (int y = 0; y < kinect.height; y += res) {
			for (int x = 0; x < kinect.width; x += res) {
				index = x + y * kinect.width;
				if (depthMap[index] > 0) {
					// get the color of the point
					pixelColor[0] = (int) red(rgbImage.pixels[index]);
					pixelColor[1] = (int) green(rgbImage.pixels[index]);
					pixelColor[2] = (int) blue(rgbImage.pixels[index]);
					stroke(pixelColor[0], pixelColor[1], pixelColor[2]);

					// draw the projected point
					realWorldPoint = new PVector(realWorldMapArray[index*3], realWorldMapArray[index*3+1], realWorldMapArray[index*3+2]);
					point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
				}
			}
		}

		// -----------------------------------------------------
		// DETERMINE CLOSEST 3D POINT TO MOUSE
		closest3DKinectPointToMouse = null;
		float minDist = Float.MAX_VALUE;
		for (int y = 0; y < kinect.height; y += res) {
			for (int x = 0; x < kinect.width; x += res) {
				index = x + y * kinect.width;
				if (depthMap[index] > 0) {
					// Get Realworld point
					realWorldPoint = new PVector(realWorldMapArray[index*3], realWorldMapArray[index*3+1], realWorldMapArray[index*3+2]);
					float sx = screenX(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
					float sy = screenY(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
					float dist = dist(sx, sy, mouseX, mouseY);
					if (dist < minDist) {
						minDist = dist;
						closest3DKinectPointToMouse = new Vec3D(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
					}
				}
			}
		}
		if (closest3DKinectPointToMouse == null) {
			System.err.println("No Closest 3dpoint to mouse.");
		}

		// -----------------------------------------------------
		// DRAW KINECT CAM
//		kinect.drawCamFrustum();

		// DRAW QUAD PLANE
		// ------------------------------------------------------
		// draw quad-points
		pushStyle();
		colorMode(HSB, 100);

		noStroke();
		sphereDetail(5);
		for (int i = 0; i < 4; i++) {
			fill(5 + 15 * i, 90, 90, 90);
			Vec3D quadPt = quadPts[i];
			if (!quadPt.equals(Vec3D.ZERO)) {
				pushMatrix();
				translate(quadPt);
				sphere(0.1f);
				popMatrix();
			}
		}
		// draw quad-rect
		stroke(0, 0, 100);
		strokeWeight(0.05f);
		fill(0, 0, 100, 30);
		if (allQuadPointsPresent) {
			beginShape();
			for (int i = 0; i < quadPts.length; i++) {
				Vec3D pt = quadPts[i];
				vertex(pt.x, pt.y, pt.z);
			}
			endShape(CLOSE);
		}
		popStyle();

		// --------------------------------------------
		// DISPLAY THRESHOLDS (method2)
		if (allQuadPointsPresent) {
			this.displayCalibrationThresholds();
		}

		popStyle();
		popMatrix();
		// END DRAW 3D CALIBRATION SCENE ---------------------------------------

	}

	public void displayCalibrationThresholds() {
		if (!allQuadPointsPresent) {
			System.err.println("Not all quad points present yet.");
			return;
		}

		pushStyle();
		colorMode(HSB, 100);

		// ~~~~~~~~~~~~~~~~~~~
		// THE FOLLOWING UTILISES AVG WIDTH, HEIGHT - HENCE ASSUMES THAT
		// QUAD OPPOSITE EDGES ARE ROUGHLY EQUAL IN LENGTH IN REAL WORLD
		// COORDINATES
		// ~ Get Average Width
		float topLength = quadPts[0].sub(quadPts[1]).magnitude();
		float btmLength = quadPts[3].sub(quadPts[2]).magnitude();
		float avgWidth = (topLength + btmLength) / 2;
		// ~ Get Average Height
		float leftLength = quadPts[1].sub(quadPts[2]).magnitude();
		float rightLength = quadPts[0].sub(quadPts[3]).magnitude();
		float avgHeight = (leftLength + rightLength) / 2;

		float maxWidthDiffQuad_Threshold = minWidthDiffQuad_Threshold + distWidthDiffQuad_Threshold;
		float maxHeightDiffQuad_Threshold = minHeightDiffQuad_Threshold + distHeightDiffQuad_Threshold;

		int index;
		PVector realWorldPoint;
		for (int y = 0; y < kinect.height; y += res) {
			for (int x = 0; x < kinect.width; x += res) {
				index = x + y * kinect.width;
				if (depthMap[index] > 0) {
					// Get Realworld point
					realWorldPoint = new PVector(realWorldMapArray[index*3], realWorldMapArray[index*3+1], realWorldMapArray[index*3+2]);

					// Get Edge Distances
					float[] edgeDistances = getDistancesFromQuadEdges(realWorldPoint);

					// Calculate total distances
					float horizontalDistanceSum = edgeDistances[0] + edgeDistances[1];
					float verticalDistanceSum = edgeDistances[2] + edgeDistances[3];

					// Thresholding
					// ~ angle between two vectors
					// ~ distance sum addition
					float widthDiff = abs(avgWidth - horizontalDistanceSum) * scale;
					float heightDiff = abs(avgHeight - verticalDistanceSum) * scale;

					// Draw Unioned Thresholds
					if (widthDiff > minWidthDiffQuad_Threshold) {
						if (widthDiff < maxWidthDiffQuad_Threshold) {
							if (heightDiff > minHeightDiffQuad_Threshold) {
								if (heightDiff < maxHeightDiffQuad_Threshold) {
									stroke(5, 90, 100);
									strokeWeight(16);
									point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
								}
							}
						}
					}

					// Draw Width Thresholds
					if (widthDiff > minWidthDiffQuad_Threshold) {
						if (widthDiff < maxWidthDiffQuad_Threshold) {
							stroke(70, 100, 100);
							strokeWeight(10);
							point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
						}
					}
					// Draw Height Thresholds
					if (heightDiff > minHeightDiffQuad_Threshold) {
						if (heightDiff < maxHeightDiffQuad_Threshold) {
							stroke(25, 100, 100);
							strokeWeight(4);
							point(realWorldPoint.x, realWorldPoint.y, realWorldPoint.z);
						}
					}

				}
			}
		}

		popStyle();
	}

	public void drawDebugText() {
		// DRAW 2D OVERLAY SCREEN
		// Draw Calibration Attributes
		pushStyle();
		colorMode(HSB, 100);
		String descript = "";
		descript += "minQuadTargettingThreshold: \t" + minQuadTargettingThreshold;
		descript += "\n";
		descript += "maxQuadTargettingThreshold: \t" + maxQuadTargettingThreshold;
		descript += "\n";
		descript += "\n";
		for (int i = 0; i < 4; i++) {
			descript += "quadPt " + i + ": \t" + quadPts[i].scale(scale);
			descript += "\n";
		}
		descript += "\n";
		descript += "FrameRate: \t" + frameRate;
		descript += "\n";
		descript += "--------------------------------------";
		descript += "\n";
		descript += "minWidthDiffQuad_Threshold: \t" + minWidthDiffQuad_Threshold;
		descript += "\n";
		descript += "distWidthDiffQuad_Threshold: \t" + distWidthDiffQuad_Threshold;
		descript += "\n";
		descript += "minHeightDiffQuad_Threshold: \t" + minHeightDiffQuad_Threshold;
		descript += "\n";
		descript += "distHeightDiffQuad_Threshold: \t" + distHeightDiffQuad_Threshold;
		descript += "\n";

		fill(0, 0, 100);
		textSize(12);
		textAlign(LEFT, TOP);
		text(descript, 10, 10);
		popStyle();
	}

	/**
	 * Get the 4 distances from a point and quad edges.
	 * 
	 * @param point
	 *            - the point from which distances to quad edges are measured
	 * @return float[4] distances = {left,right,top,bottom}
	 */
	public float[] getDistancesFromQuadEdges(PVector point) {
		float[] distances = new float[4];

		// Get Quad's points
		PVector q1 = new PVector(quadPts[0].x, quadPts[0].y, quadPts[0].z);
		PVector q2 = new PVector(quadPts[1].x, quadPts[1].y, quadPts[1].z);
		PVector q3 = new PVector(quadPts[2].x, quadPts[2].y, quadPts[2].z);
		PVector q4 = new PVector(quadPts[3].x, quadPts[3].y, quadPts[3].z);

		// Calculate distances from left, right, top, bottom edges
		distances[0] = pointLineDistance3D(q1, q4, point);
		distances[1] = pointLineDistance3D(q2, q3, point);
		distances[2] = pointLineDistance3D(q1, q2, point);
		distances[3] = pointLineDistance3D(q4, q3, point);

		return distances;
	}

	// @formatter:off
	/**
	 * Returns closest point on the quad surface to a particular testPoint.
	 * 
	 * @param testPoint
	 * @return Vec3D[] closestPoint_asRealWorldAndPercentage where
	 *         closestPoint[0] is the realWorld representation, and
	 *         closestPoint[1] is the Percentage of quad (width,and
	 *         representation.
	 */
	// @formatter:on
	public Vec3D[] closestQuadHitTarget_asRealWorldAndPercentage(Vec3D testPoint) {
		Vec3D q1 = quadPts[0];
		Vec3D q2 = quadPts[1];
		Vec3D q3 = quadPts[2];
		Vec3D q4 = quadPts[3];

		float x, y, z;
		// for every 'column' (x, horizontal)
		// for every 'row' (y, vertical)

		float minDistance = Float.MAX_VALUE;
		Vec3D closestPoint_asRealWorld = null;
		Vec3D closestPoint_asPercentage = new Vec3D();

		for (float h = 0; h < 1; h += quadRes) {
			// Determine topColumnPosition
			x = map(h, 0, 1, q1.x, q2.x);
			y = map(h, 0, 1, q1.y, q2.y);
			z = map(h, 0, 1, q1.z, q2.z);
			Vec3D topColumnPos = new Vec3D(x, y, z);
			// Determine bottomColumnPosition
			x = map(h, 0, 1, q4.x, q3.x);
			y = map(h, 0, 1, q4.y, q3.y);
			z = map(h, 0, 1, q4.z, q3.z);
			Vec3D btmColumnPos = new Vec3D(x, y, z);

			for (float v = 0; v < 1; v += quadRes) {
				// Determine curPosition
				x = map(v, 0, 1, topColumnPos.x, btmColumnPos.x);
				y = map(v, 0, 1, topColumnPos.y, btmColumnPos.y);
				z = map(v, 0, 1, topColumnPos.z, btmColumnPos.z);
				Vec3D curPos = new Vec3D(x, y, z);

				float distance = curPos.distanceTo(testPoint);
				if (distance < minDistance) {
					minDistance = distance;
					closestPoint_asRealWorld = curPos.copy();
					closestPoint_asPercentage.x = h; // ~
					closestPoint_asPercentage.y = v; // ~
				}
			}
		}

		Vec3D[] closestPoint_asRealWorldAndPercentage = { closestPoint_asRealWorld, closestPoint_asPercentage };

		return closestPoint_asRealWorldAndPercentage;
	}

	public void keyPressed() {
		// QUAD CALIBRATION CONTROL ---------------------------------------
		// for numbers 1,2,3,4 - set the quad-boundary position
		if (key == '1')
			quadPts[0] = closest3DKinectPointToMouse.copy();
		if (key == '2')
			quadPts[1] = closest3DKinectPointToMouse.copy();
		if (key == '3')
			quadPts[2] = closest3DKinectPointToMouse.copy();
		if (key == '4')
			quadPts[3] = closest3DKinectPointToMouse.copy();

		// Change res'
		if (key == 'o') {
			res--;
			res = max(res, 1);
			println("res: " + res);
		}
		if (key == 'p') {
			res++;
			println("res: " + res);
		}
		if (key == 'k') {
			quadRes -= 0.01f;
			quadRes = max(quadRes, 0.01f);
			println("quadRes: " + quadRes);
		}
		if (key == 'l') {
			quadRes += 0.01f;
			quadRes = min(quadRes, 0.5f);
			println("quadRes: " + quadRes);
		}
		int dt = 2;
		// Change threshold
		if (key == 'u') {
			minWidthDiffQuad_Threshold -= dt;
		}
		if (key == 'U') {
			minWidthDiffQuad_Threshold += dt;
		}
		if (key == 'i') {
			distWidthDiffQuad_Threshold -= dt;
		}
		if (key == 'I') {
			distWidthDiffQuad_Threshold += dt;
		}
		if (key == 'h') {
			minHeightDiffQuad_Threshold -= dt;
		}
		if (key == 'H') {
			minHeightDiffQuad_Threshold += dt;
		}
		if (key == 'j') {
			distHeightDiffQuad_Threshold -= dt;
		}
		if (key == 'J') {
			distHeightDiffQuad_Threshold += dt;
		}

		// Cap Threshold Values
		minHeightDiffQuad_Threshold = Math.max(minHeightDiffQuad_Threshold, 0);
		minWidthDiffQuad_Threshold = Math.max(minWidthDiffQuad_Threshold, 0);
		distWidthDiffQuad_Threshold = Math.max(distWidthDiffQuad_Threshold, 0);
		distHeightDiffQuad_Threshold = Math.max(distHeightDiffQuad_Threshold, 0);

		// Reset Camera to default position
		if (key == 'c') {
			cam.reset();
		}
		// ////////////

	}

	// ----------------
	// HELPER FUNCTIONS
	void translate(Vec3D v) {
		translate(v.x, v.y, v.z);
	}

	/**
	 * Shortest distance from 2 lines and a point.
	 * 
	 * @param linePt1
	 * @param linePt2
	 * @param point
	 * @return
	 */
	public float pointLineDistance3D(PVector linePt1, PVector linePt2, PVector point) {
		// Formula from:
		// http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
		PVector a = PVector.sub(point, linePt1);
		PVector b = PVector.sub(point, linePt2);
		PVector c = PVector.sub(linePt2, linePt1);
		PVector crossAB = a.cross(b);

		float distance = crossAB.mag() / c.mag();

		return distance;
	}

	/**
	 * same as map() function, except values are capped at start2 and stop2
	 * 
	 * @param value
	 * @param start1
	 * @param stop1
	 * @param start2
	 * @param stop2
	 * @return mapped and capped value
	 */
	public float mapWithCap(float value, float start1, float stop1, float start2, float stop2) {
		float v = map(value, start1, stop1, start2, stop2);
		float biggerValue = Math.max(start2, stop2);
		float smallerValue = Math.min(start2, stop2);
		v = Math.min(biggerValue, v);
		v = Math.max(smallerValue, v);
		return v;
	}

}
