import processing.video.*;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
 
Capture capture;
PImage checker;
Mat cameraMat, checkerMat, trackMat, resultMat, maskMat;
Rect maskRect;
int w = 640, h = 480;
double tracking_threshold = -0.70;
 
void setup() {
  size(640, 480);
  ellipseMode(CENTER);
  stroke(211, 99 ,42);
  strokeWeight(2);

  System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  println("Loaded OpenCV " + Core.VERSION);
  
  capture = new Capture(this, w, h);
  capture.start();
  checker = loadImage("checker16.png");   
  checkerMat = toMat(checker);
  trackMat = new Mat(h-checker.height+1, w-checker.width+1, CvType.CV_32FC1);
  resultMat = new Mat(h-checker.height+1, w-checker.width+1, CvType.CV_8UC4);
  maskMat = new Mat();
  maskRect = new Rect();
}
 
void draw() {
  image(capture, 0, 0);
  
  cameraMat = toMat(capture);
  Imgproc.matchTemplate(cameraMat, checkerMat, trackMat, Imgproc.TM_CCOEFF_NORMED);
  Imgproc.threshold(trackMat, trackMat, tracking_threshold, 1, Imgproc.THRESH_TOZERO_INV);
  
//  Core.normalize(trackMat, trackMat, 0, 1, Core.NORM_MINMAX, -1);
//  
//  PImage track = toPImage(trackMat);
//  image(track, w, 0);

  
  for (int i = 0; i < 4; i++) {
    Core.MinMaxLocResult trackLoc = Core.minMaxLoc(trackMat);
  
    if (trackLoc.minVal <= tracking_threshold) {
      print(trackLoc.minVal + " ");
      ellipse((float)trackLoc.minLoc.x + checker.width/2, (float)trackLoc.minLoc.y + checker.height/2, 10, 10);
      Imgproc.floodFill(trackMat, maskMat, trackLoc.minLoc, Scalar.all(1), maskRect, Scalar.all(0.1), Scalar.all(1.0), 4);
    } else {
      print(trackLoc.minVal + " ");
      break; 
    }
  }
  println();
  
  text("Frame Rate: " + round(frameRate), 500, 50);
}

void captureEvent (Capture c) {
  c.read(); 
}
