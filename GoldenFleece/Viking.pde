// ----------------------------------------------------
// VIKING TAB

public class Viking {
  PVector pos;
  PVector size;
  HashMap<String, Sprite> animations;
  String state;
  String type;
  int damage;
  int life; // Mini boss only
  int max_life = 3; // Mini boss only

  Sprite[] body_parts;
  float[] angular_velocities;
  PVector[] blast_forces;
  float[] ground_level;
  PVector knockback_force;
  float tint_value;

  Viking(String type) {
    animations = new HashMap<String, Sprite>();
    pos = new PVector(); // Avoid Null Pointer
    body_parts = new Sprite[8];
    angular_velocities = new float[8];
    blast_forces = new PVector[8];
    ground_level = new float[8];
    for (int i = 0; i < blast_forces.length; i++) {
      blast_forces[i] = new PVector(0, 0);
    }
    knockback_force = new PVector(0, 0);
    this.type = type;

    // Run Animation
    Sprite run_sprite;
    if (type == "main") {
      run_sprite = new Sprite("viking_run", "Vikings/Main_Viking/VikingRun%d.png", 1, 8, 4);
      damage = 20;
    } else if (type == "alt") {
      run_sprite = new Sprite("viking_alt_run", "Vikings/MainAlt_Viking/VikingRun%d.png", 1, 8, 4);
      damage = 20;
    } else if (type == "bomba") {
      run_sprite = new Sprite("bomba_run", "Vikings/Bomba_Viking/VikingBomba_Run%d.png", 1, 8, 4);
      damage = 50;
    } else if (type == "horse") {
      run_sprite = new Sprite("horse_run", "Vikings/Horse_Viking/HorseViking_Run%d.png", 1, 8, 4);
      damage = 10;
    } else if (type == "flag") {
      run_sprite = new Sprite("flag_run", "Vikings/Flag_Viking/VikingFlag_Run%d.png", 1, 8, 4);
      damage = 10;
    } else if (type == "miniboss") {
      run_sprite = new Sprite("miniboss_run", "Vikings/MiniBoss_Viking/RunMiniBoss%d.png", 1, 8, 4);
      damage = 90;
    } else {
      run_sprite = new Sprite("viking_run", "Vikings/Main_Viking/Run/VikingRun%d.png", 1, 8, 4);
      damage = 20;
    }
    run_sprite.randomFrame();
    run_sprite.loopAnimation(true);
    animations.put("run", run_sprite);
    this.state = "run";
    this.size = run_sprite.getSize();

    // Scared Running Animation
    Sprite run_scared_sprite;
    if (type == "main") {
      run_scared_sprite = new Sprite("viking_run_scared", "Vikings/Main_Viking/VikingRunScared%d.png", 1, 8, 4);
    } else if (type == "alt") {
      run_scared_sprite = new Sprite("viking_alt_run_scared", "Vikings/MainAlt_Viking/VikingRunScared%d.png", 1, 8, 4);
    } else {
      run_scared_sprite = new Sprite("viking_run_scared", "Vikings/Main_Viking/VikingRunScared%d.png", 1, 8, 4);
    }
    run_scared_sprite.randomFrame();
    run_scared_sprite.loopAnimation(true);
    animations.put("run_scared", run_scared_sprite);

    // Attack Animation
    Sprite attack_sprite;
    if (type == "main") {
      attack_sprite = new Sprite("viking_attack", "Vikings/Main_Viking/AttackViking%d.png", 1, 10, 4);
    } else if (type == "alt") {
      attack_sprite = new Sprite("viking_alt_attack", "Vikings/MainAlt_Viking/AttackViking%d.png", 1, 10, 4);
    } else if (type == "miniboss") {
      attack_sprite = new Sprite("miniboss_attack", "Vikings/MiniBoss_Viking/Attack_miniBoss%d.png", 1, 9, 4);  
    } else {
      attack_sprite = new Sprite("viking_attack", "Vikings/Main_Viking/AttackViking%d.png", 1, 10, 4);
    }
    animations.put("attack", attack_sprite);

    // GOLD! GOLD! GOLD!
    Sprite gold_sprite;
    if (type == "main") {
      gold_sprite = new Sprite("viking_gold", "Vikings/Main_Viking/VikingRun_Coin%d.png", 1, 8, 4);
    } else if (type == "alt") {
      gold_sprite = new Sprite("viking_alt_gold", "Vikings/MainAlt_Viking/VikingRun_Coin%d.png", 1, 8, 4);
    } else if (type == "horse") {
      gold_sprite = new Sprite("viking_horse_gold", "Vikings/Horse_Viking/HorseViking_Run_Coin%d.png", 1, 8, 4);
    } else if (type == "flag") {
      gold_sprite = new Sprite("viking_flag_gold", "Vikings/Flag_Viking/VikingFlag_Run_Coin%d.png", 1, 8, 4);
    } else if (type == "miniboss") {
      gold_sprite = new Sprite("miniboss_gold", "Vikings/MiniBoss_Viking/RunMiniBoss_WithCoin%d.png", 1, 8, 4);
    } else {
      gold_sprite = new Sprite("viking_gold", "Vikings/Main_Viking/VikingRun_Coin%d.png", 1, 8, 4);
    }
    gold_sprite.randomFrame();
    gold_sprite.loopAnimation(true);
    animations.put("gold", gold_sprite);

    // Sprites for gory bits
    for (int i = 1; i <= 8; i++) {
      String name = "viking_bits_%d";
      name = name.replace("%d", nf(i, 4));
      String filename = "Vikings/Bits/ParticleBits%d.png";
      filename = filename.replace("%d", nf(i, 4));
      body_parts[i - 1] = new Sprite(name, filename);
    }
    tint_value = 0;
    if (this.type == "miniboss") {
      this.life = max_life; 
    } else {
      this.life = 1; 
    }
  }

  public void addAnimation(String state, Sprite animation) {
    animations.put(state, animation);
  }

  public Sprite getAnimation() {
    return animations.get(this.state);
  }

  public Sprite getAnimation(String state) {
    return animations.get(state);
  }

  public void setPos(PVector pos) {
    this.pos = pos;
  }

  public void setState(String state) {
    this.state = state;
  }

  public void advance(float distance, float confidence) {
    if (this.type == "horse") {
      distance *= 2;
    } else if (this.type == "miniboss") {
      distance /= 2; 
    }
    float r = random(1);
    if (this.state == "run") {
      if (r <= confidence / 2) {
        this.pos.x -= distance * 2;
      } else if (r <= confidence) {
        this.pos.x -= distance;
      } else {
        this.pos.x -= distance / 2;
      }
    } else if (this.state == "run_scared") {
      distance *= 2;
      if (r <= confidence / 2) {
        this.pos.x -= distance * 2;
      } else if (r <= confidence) {
        this.pos.x -= distance;
      } else {
        this.pos.x -= distance / 2;
      }
    }
    if (this.state == "run" || this.state == "run_scared") {
      // Path to victory
      if (this.pos.x > 1000 && this.pos.y < 750) { // boat line
        this.pos.y++;
      }
      if (this.pos.x > 800 && this.pos.x < 1200 && this.pos.y > 600) { // bush line
        this.pos.y -= distance / 2;
      }
      if (this.pos.x < 500 && this.pos.y > 550) {
        this.pos.y -= random(3) + distance / 2;
      }
      if (r / 2 <= confidence || abs(this.pos.x / width) > this.pos.y / abs(treasure_line - this.pos.y)) {
        if (this.pos.y > treasure_line) {
          this.pos.y -= random(0.5f, 1);
        } else if (this.pos.y < treasure_line) {
          this.pos.y += (distance - 1);
        }
      }
    }
    if (this.state == "gold") {
      if (r <= confidence / 2) {
        this.pos.x += distance * 2;
      } else if (r <= confidence) {
        this.pos.x += distance;
      } else {
        this.pos.x += distance / 2;
      }
      this.pos.y += distance / 5;
    }
  }

  public void update() {
    if (this.state != "hit") {
      for (int i = 0; i < body_parts.length; i++) {
        body_parts[i].pos = new PVector(this.pos.x, this.pos.y);
      }
    }

    Sprite s = this.getAnimation();
    if (this.state == "run" || this.state == "run_scared") {
      this.pos.add(this.knockback_force);
      if (this.pos.y < treasure_line - 10) {
        this.pos.y = treasure_line - 10;
      } else if (this.pos.y > height - 50) {
        this.pos.y = height - 50;
      }
      this.knockback_force.mult(0.9f);
      s.update();

      if (this.pos.dist(new PVector(ram_x, ram_y)) < 100) {
        if (this.type == "main" || this.type == "alt") {
          this.state = "attack";
        } else if (this.type == "flag" || this.type == "horse") {
          this.state = "gold";
          Sprite new_s = this.getAnimation();
          new_s.flipHorizontal();
          ramDamage(this.damage);
        } else if (this.type == "bomba") {
          flame_explosion_sfx.trigger();
          addFlameExplosion(this.pos);
          hitEvent(this.pos.x, this.pos.y);
          ramDamage(this.damage);
        }
      }
    } else if (this.state == "hit") {
      for (int i = 0; i < body_parts.length; i++) {
        PVector force = blast_forces[i];
        Sprite part = body_parts[i];
        PVector part_pos = new PVector(part.pos.x, part.pos.y);
        part_pos.add(force);
        part.angle += angular_velocities[i];
        if (part_pos.y > ground_level[i]) {
          part_pos.y = ground_level[i];
          force.x *= 0.5;
          angular_velocities[i] *= 0.5;
        } else {
          force.y += 1;
          angular_velocities[i] *= 0.9;
        }
        part.setPos(part_pos);
      }

      // Tint sprites
      tint_value++;
    } else if (this.state == "attack") {
      s.update();
      if (s.isFinished()) { // Attack landed
        this.state = "gold";
        Sprite new_s = this.getAnimation();
        new_s.flipHorizontal();
        ramDamage(this.damage);
      }
    } else if (this.state == "gold") {
      s.update();
    }
  }

  public void draw() {
    Sprite s = this.getAnimation();
    if (this.state == "run" || this.state == "run_scared" || this.state == "attack" || this.state == "gold") {
      pushMatrix();
      pushStyle();
      if (this.type == "miniboss") {
        tint(3, (this.max_life-this.life)*20, 100);
      }
      s.draw(this.pos);
      popStyle();
      popMatrix();
    } else if (this.state == "hit") {
      pushStyle();
      tint(3, 100, max(150 - this.tint_value, 100), min(max(150 - this.tint_value, 0), 100));
      for (Sprite part : body_parts) {
        part.draw();
      }
      popStyle();
    }
  }

  public void hit(PVector target) {
    if (this.state == "hit") {
      return;
    }

    stat_pbKills++;

    life--;
    if (life > 0) {
      return; 
    }



    if (this.state == "gold") {
      int d = this.damage;
      if (this.type == "main" || this.type == "alt") {
        d /= 2;
      }
      ramDamage(-d);
    }
    this.state = "hit";
    // Body part forces initialisation
    for (int i = 0; i < blast_forces.length; i++) {
      blast_forces[i] = new PVector(target.x, target.y);
      blast_forces[i].sub(this.pos);
      blast_forces[i].normalize();
      blast_forces[i].x *= random(-20, 20);
      blast_forces[i].y = -random(10, 25);
      angular_velocities[i] = random(-1, 1);
    }
    Sprite splat = addBloodsplat(this.pos);

    if (target.x > this.pos.x) {
      splat.flipHorizontal();
    }
    for (int i = 0; i < ground_level.length; i++) {
      this.ground_level[i] = this.pos.y + this.size.y / 2 + randomGaussian() * 50;
    }
    if (this.type == "bomba") {
      flame_explosion_sfx.trigger();
      addFlameExplosion(this.pos);
      hitEvent(this.pos.x, this.pos.y);
    }
  }

  public void knockback(PVector target) {
    float distance;
    PVector new_force = new PVector(target.x, target.y);
    new_force.sub(this.pos);
    distance = new_force.mag();
    new_force.normalize();
    new_force.mult((knockback_radius - distance) * 0.1f);
    new_force.y *= 0.75;
    this.knockback_force.add(new_force);
  }

  public boolean isFinished() {
    return tint_value >= 150;
  }
}

