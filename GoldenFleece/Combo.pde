// ----------------------------------------------------
// COMBO TAB
public class ComboDisplay {
  // String comboText;
  int lifeSpan = 30;
  int curAge = 0;
  boolean isAlive = true;

  PVector pos;

  float randomHue;

  String comboNumber;

  ComboDisplay(String comboNumber) {
    this.comboNumber = comboNumber;
    // comboText = "COMBO x " + comboNumber;
    pos = new PVector(swidth * 0.5f, sheight * 0.35f);
    randomHue = random(100);
  }

  void update() {
    if (isAlive) {
      curAge++;
      if (curAge > lifeSpan) {
        isAlive = false;
      }
    }
  }

  void draw() {
    float scaleInProgress = map(curAge, 0, lifeSpan * 0.25f, 0, 1);
    scaleInProgress = min(scaleInProgress, 1);
    scaleInProgress = max(scaleInProgress, 0);

    float riseUpProgress = map(curAge, lifeSpan / 2, lifeSpan, 0, 1);
    riseUpProgress = min(riseUpProgress, 1);
    riseUpProgress = max(riseUpProgress, 0);

    float scaleAmount = elasticValueAtPctgProgress(scaleInProgress);
    float alpha = (1 - riseUpProgress) * 100;
    float yoffset = -riseUpProgress * 40;

    pushMatrix();
    pushStyle();
    translate(pos.x, pos.y);
    translate(40 * 2, 0); // offsetX
    translate(0, yoffset);
    scale(scaleAmount);
    textFont(customFont, 36);
    // Draw Text
    String descript1, descript2, descript3;
    fill(randomHue, 90, 90, alpha);

    descript1 = "COMBO   ";
    textSize(40);
    textAlign(RIGHT, CENTER);
    text(descript1, 0, 0);

    descript2 = "X";
    textSize(60);
    textAlign(CENTER, CENTER);
    text(descript2, 0, 0);

    descript3 = "   " + comboNumber;
    textSize(40);
    textAlign(LEFT, CENTER);
    text(descript3, 0, 0);

    // SECOND DRAW
    translate(-5, -5);
    fill(0, 0, 100, alpha);

    descript1 = "COMBO   ";
    textSize(40);
    textAlign(RIGHT, CENTER);
    text(descript1, 0, 0);

    descript2 = "X";
    textSize(60);
    textAlign(CENTER, CENTER);
    text(descript2, 0, 0);

    descript3 = "   " + comboNumber;
    textSize(40);
    textAlign(LEFT, CENTER);
    text(descript3, 0, 0);

    popStyle();
    popMatrix();
  }
}

// ----------------
// HELPER FUNCTIONS
/**
 * same as map() function, except values are capped at start2 and stop2
 * 
 * @param value
 * @param start1
 * @param stop1
 * @param start2
 * @param stop2
 * @return
 */
public float mapWithCap(float value, float start1, float stop1, float start2, float stop2) {
  float v = map(value, start1, stop1, start2, stop2);
  float biggerValue = Math.max(start2, stop2);
  float smallerValue = Math.min(start2, stop2);
  v = Math.min(biggerValue, v);
  v = Math.max(smallerValue, v);
  return v;
}

public float elasticValueAtPctgProgress(float x) {
  // the original value can be represented as 1 - at x=1, elasticValue=1
  // ~ rough guess of a curve that can simulate elastic transitions - e.g. bounces
  // 1st version
  // float y = (x - 0.8f) * (x - 0.8f) + 0.95f;
  // 2nd version
  float y = Math.abs(x * x - 0.8f) + 0.8f;
  return y;
}

