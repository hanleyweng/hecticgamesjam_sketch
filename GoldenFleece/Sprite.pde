// ----------------------------------------------------
// SPRITE TAB

HashMap<String, PImage[]> sprite_cache = new HashMap<String, PImage[]>();

public void addSprite(String assetName, String filename, int start, int end, int padding) {
  PImage[] frames = new PImage[end];
  for (int i = 0; i <= end - start; i++) {
    String name = filename.replace("%d", nf(start + i, padding));
    frames[i] = loadImage(name);
  }
  sprite_cache.put(assetName, frames);
}

public void addSprite(String assetName, String filename) {
  PImage[] frames = new PImage[1];
  frames[0] = loadImage(filename);
  sprite_cache.put(assetName, frames);
}

public class Sprite {
  PVector pos;
  float angle;
  PVector scale;
  String asset;
  int framecount;
  int speed;
  int start;
  int end;
  int len;
  int life;
  boolean animated;
  boolean looping;

  Sprite(String asset, String filename) {
    if (!sprite_cache.containsKey(asset)) {
      addSprite(asset, filename);
    }
    this.asset = asset;
    this.framecount = 0;
    this.speed = 0;
    this.start = 0;
    this.end = 0;
    this.len = 0;
    this.animated = true;
    this.looping = false;
    this.angle = 0;
    this.life = -1;
    this.scale = new PVector(1, 1);
  }

  Sprite(String asset, String filename, int start, int end, int padding) {
    // Caching sprites so we don't ever load the same images twice
    if (!sprite_cache.containsKey(asset)) {
      addSprite(asset, filename, start, end, padding);
    }
    this.asset = asset;
    this.framecount = 0;
    this.speed = 1;
    this.start = start;
    this.end = end;
    this.len = end - start;
    this.animated = true;
    this.looping = false;
    this.angle = 0;
    this.life = -1;
    this.scale = new PVector(1, 1);
  }

  public void setPos(PVector pos) {
    this.pos = pos;
  }

  public void setLife(int frames) {
    this.life = frames;
  }

  public void flipHorizontal() {
    this.scale.x *= -1;
  }

  public void rewind() {
    this.framecount = 0;
  }

  public void randomFrame() {
    this.framecount = (int) random(this.len);
  }

  public void loopAnimation(boolean looping) {
    this.looping = looping;
  }

  public void animate(boolean animated) {
    this.animated = animated;
  }

  public void update() {
    if (animated) {
      if (looping) {
        this.framecount = (this.framecount + 1) % this.len;
      } else {
        this.framecount = min(this.framecount + this.speed, this.len);
      }
    }
    if (this.life >= 0) {
      this.life--;
    }
  }

  public PVector getSize() {
    PImage frame = this.getFrame();
    return new PVector(frame.width, frame.height);
  }

  public PImage getFrame() {
    if (looping) {
      return sprite_cache.get(this.asset)[this.framecount % this.len];
    } else {
      return sprite_cache.get(this.asset)[this.framecount];
    }
  }

  public void draw() {
    this.draw(this.pos);
  }
  
  // Messy, I know
  public void drawOriginalSize(PVector pos) {
    PImage frame = this.getFrame();
    int w = frame.width;
    int h = frame.height;
    pushMatrix();
    pushStyle();
    translate(pos.x, pos.y);
    rotate(this.angle);
    scale(this.scale.x, this.scale.y); // Hack to draw smaller pixels
    if (life >= 0) {
      tint(100, 0, 100, min(life, 100));
    }
    image(frame, -w / 2, -h / 2);
    popStyle();
    popMatrix();
  }

  public void draw(PVector pos) {
    PImage frame = this.getFrame();
    int w = frame.width;
    int h = frame.height;
    pushMatrix();
    pushStyle();
    translate(pos.x, pos.y);
    rotate(this.angle);
    scale(this.scale.x * 0.75f, this.scale.y * 0.75f); // Hack to draw smaller pixels
    if (life >= 0) {
      tint(100, 0, 100, min(life, 100));
    }
    image(frame, -w / 2, -h / 2);
    popStyle();
    popMatrix();
  }

  public boolean isFinished() {
    if (this.life >= 0) {
      return this.life == 0;
    } else {
      return this.framecount >= this.end - this.start;
    }
  }
}

