// ----------------------------------------------------
// DEFAULT TAB

import java.util.*;
import ddf.minim.*;
import processing.video.*;
import oscP5.*;

// COMMUNICATION VARS
int portToListenToo = 12000;
OscP5 oscP5;
boolean kinectInputAllowed = true;

// In-Game Vars
int fps = 30;
int window_width = 1280;
int window_height = 800;
int bg_x = -55;
int bg_y = -80;
int fg_x = 0;
int fg_y = 660;
int fg_tree_x = 65;
int fg_tree_y = 650;
int goldbar_x = 200;
int goldbar_y = 50;
int ram_x = 175;
int ram_y = 425;
int spawn_interval_initial = 180;
int spawn_interval = spawn_interval_initial;
float initialLife = 100; //10000000; //10000; //100;
float life = initialLife;

int screenshake_frames = 0;
float screenshakeX = 0;
float screenshakeY = 0;
float knockback_radius = 50;
float treasure_line = 500;
boolean isRamHit = false;

boolean is_explosion_queued = false;

// Cutscene Sprites
PImage bg_cutscene;
Sprite sheep;
Sprite ships;

// In-game Sprites
ArrayList<Sprite> trees;
ArrayList<Sprite> treetop_leaves;
ArrayList<Sprite> explosions;
ArrayList<Sprite> craters;
ArrayList<Sprite> flame_explosions;
ArrayList<Sprite> bloodsplats;
ArrayList<Sprite> clouds;
ArrayList<Sprite> leaves;
Sprite bush1;
Sprite bush2;
Sprite ram;
Sprite fg_tree;
ArrayList<Squad> vikings;
Sprite goldbar_frame;
Sprite goldbar;
Sprite goldbar_bg;
PImage fg;
PImage bg;

Minim minim;
AudioSample explosion_sfx;
AudioSample flame_explosion_sfx;

// GAMEMODE VARIABLES
String GAMEMODE_SCORESCREEN = "GAMEMODE_SCORESCREEN";
String GAMEMODE_PLAYSCREEN = "GAMEMODE_PLAYSCREEN";
String GAMEMODE_CUTSCENE = "GAMEMODE_CUTSCENE";
String GAMEMODE_STARTSCREEN = "GAMEMODE_STARTSCREEN";

PlayButton playBtn;
int prv_stat_pbKills = 0;
int stat_pbKills = 0;
int stat_gbKills = 0; // globally most kills
int stat_gbCombo = 0; // global best combo
int stat_pbCombo = 0; // personal best combo
int stat_curCombo = 0;
PImage gameOverPopup;
PImage gameOverBase;
PImage gameOverBest;
PImage gameOverCombo;
PImage gameOverGameOver;
PImage gameOverKills;
PImage gameOverTapReplay;

PImage logo;

ComboDisplay singleComboDisplay;
ArrayList<ComboDisplay> comboDisplays = new ArrayList<ComboDisplay>();

String gameMode = GAMEMODE_CUTSCENE; // Starting game mode
int swidth = window_width;
int sheight = window_height;
int screenFrameCount = 0;
int spawn_rate = 6;
int base_speed = 2;

int minComboToDisplay = 9;

AudioSample game_over_sfx;
AudioSample playBtn_sfx;
AudioSample painOw_sfx;
AudioSample painStolen_sfx;
AudioPlayer electroHorn_audio;
AudioPlayer scoreScreen_audio;

PVector latestHitEvent;

PFont customFont;
PFont bebasFont;

public void setup() {
  size(window_width, window_height);
  frameRate(fps);
  colorMode(HSB, 100);
  ellipseMode(RADIUS);

  minim = new Minim(this);
  explosion_sfx = minim.loadSample("Explosion.mp3", 1024);
  flame_explosion_sfx = minim.loadSample("FlamingExplosion.mp3", 1024);

  bg_cutscene = loadImage("Background_NoShips.png");
  sheep = new Sprite("sheep", "Cutscene/SheepAnimationChopped/SheepAnimation%d.png", 1, 240, 4);
  ships = new Sprite("ships", "Cutscene/ShipAnimation/ShipAnimation%d.png", 1, 20, 4);
  sheep.setPos(new PVector(80, 550));
  ships.setPos(new PVector(1120, 360));

  trees = new ArrayList<Sprite>();
  treetop_leaves = new ArrayList<Sprite>();
  explosions = new ArrayList<Sprite>();
  flame_explosions = new ArrayList<Sprite>();
  craters = new ArrayList<Sprite>();
  bloodsplats = new ArrayList<Sprite>();
  clouds = new ArrayList<Sprite>();
  leaves = new ArrayList<Sprite>();
  ram = new Sprite("ram", "Sheepy/Sheepy%d.png", 1, 5, 4);
  ram.setPos(new PVector(ram_x, ram_y));
  ram.framecount = 4;
  ram.animate(false);
  fg_tree = new Sprite("fg_tree", "FX/Foreground_Tree/Foreground_Tree%d.png", 1, 7, 4);
  fg_tree.setPos(new PVector(fg_tree_x, fg_tree_y));
  fg_tree.framecount = 7;
  vikings = new ArrayList<Squad>();
  goldbar_frame = new Sprite("goldbar_frame", "HealthBar.png");
  goldbar = new Sprite("goldbar", "Bar.png");
  goldbar_bg = new Sprite("goldbar_bg", "bar_BG.png");
  goldbar_frame.pos = new PVector(goldbar_x, goldbar_y);
  goldbar.pos = new PVector(goldbar_x + 18, goldbar_y + 1); // Hack but works
  goldbar_bg.pos = new PVector(goldbar_x + 18, goldbar_y + 1);
  fg = loadImage("Foreground_Bush.png");
  bg = loadImage("LatestBG.png");

  playBtn = new PlayButton();
  playBtn.location = new PVector(swidth / 2, sheight * 0.85f);
  playBtn.dimensions = new PVector(250, 80);
  gameOverPopup = loadImage("GameOverScreen_Elements/GameOverScreen_AllElements.png");
  gameOverBase = loadImage("GameOverScreen_Elements/gover_base.png");
  gameOverBest = loadImage("GameOverScreen_Elements/gover_best.png");
  gameOverCombo = loadImage("GameOverScreen_Elements/gover_combo.png");
  gameOverGameOver = loadImage("GameOverScreen_Elements/gover_GameOver.png");
  gameOverKills = loadImage("GameOverScreen_Elements/gover_kills.png");
  gameOverTapReplay = loadImage("GameOverScreen_Elements/gover_tapPlay.png");

  logo = loadImage("Logo_Shadow.png");

  gameOverPopup = gameOverBase;

  // Battle arena ingame 2 (theme).mp3
  electroHorn_audio = minim.loadFile("Battle arena ingame 2 (theme).mp3", 1024);// electroHorn.mp3", 1024); //main audio
  electroHorn_audio.rewind();
  electroHorn_audio.loop();
  scoreScreen_audio = minim.loadFile("electroHorn.mp3", 1024);// "scoreScreenMusic.mp3", 1024);
  scoreScreen_audio.setGain(-10);
  scoreScreen_audio.pause();
  game_over_sfx = minim.loadSample("gameOver_Sound.mp3", 1024);
  playBtn_sfx = minim.loadSample("HGJam_PlayBtn_sfx.mp3", 1024);
  painOw_sfx = minim.loadSample("HGJ_PainOw_sfx.mp3", 1024);
  painStolen_sfx = minim.loadSample("HGJ_PainStolen_sfx.mp3", 1024);

  addClouds();
  addBushes();
  addTree(new PVector(800, 420));
  addTree(new PVector(870, 430));
  addTree(new PVector(830, 440));
  addTree(new PVector(360, 435));
  addTree(new PVector(430, 415));
  addTreetopLeaves();

  customFont = loadFont("8BITWONDERNominal-36.vlw");
  bebasFont = loadFont("Bebas-36.vlw");

  resetVariablesForNewGame();

  this.changeGameModeTo(GAMEMODE_CUTSCENE);

  // Setup OSC Communication
  // Listen for incoming messages
  oscP5 = new OscP5(this, portToListenToo);
}

public void resetVariablesForNewGame() {
  // Reset Stats
  stat_pbKills = 0;
  stat_pbCombo = 0;

  // TODO - H: may have missed some reset values here - some functions duplicated from void setup
  life = initialLife;
  spawn_interval = spawn_interval_initial;
  vikings = new ArrayList<Squad>();

  ram = new Sprite("ram", "Sheepy/Sheepy%d.png", 1, 5, 4);
  ram.setPos(new PVector(ram_x, ram_y));
  ram.framecount = 4;
  ram.animate(false);
}

public void update() {
  // update stat
  calculateCombos();

  if ((screenFrameCount + spawn_interval - 15) % spawn_interval == 0) {
    vikings.add(new Squad(new PVector(width + 50, 780 + random(50)), spawn_rate, 200, 10));
    spawn_interval -= 1;
    if (spawn_interval < 120) {
      spawn_interval = 120;
    }
  }
  if ((screenFrameCount - 1) % (int) random(240, 500) == 0) {
    Sprite leaf = new Sprite("leaf", "FX/LeafParticle.png");
    leaf.setPos(new PVector(width + 100, random(100)));
    leaf.angle = random(2 * PI);
    leaves.add(leaf);
  }
}

public void updateRegardless() {
  update();

  for (Sprite cloud : clouds) {
    cloud.pos.x -= 0.5f;
    if (cloud.pos.x < -200) {
      cloud.pos.x = width + 200;
    }
  }
}

public void drawRegardless() {
  // ////////////////////////////////////////////////////////////
  // SCREENSHAKE
  makeScreenShake();

  // ////////////////////////////////////////////////////////////
  // BACKGROUND
  pushMatrix();
  pushStyle();
  if (is_explosion_queued) {
    tint(3, 25, 100);
    image(bg, bg_x, bg_y);

    explosion_sfx.trigger();
    addScreenShake((int) random(60));
    for (Sprite t : trees) {
      t.rewind();
      t.framecount += (int) random(3);
    }
    for (Sprite t : treetop_leaves) {
      t.rewind();
      t.framecount += (int) random(5);
    }
    bush1.rewind();
    bush2.rewind();
    bush1.framecount += (int) random(2);
    bush2.framecount += (int) random(2);
    fg_tree.rewind();
    fg_tree.framecount += (int) random(2);
    is_explosion_queued = false;
  } else {
    if (gameMode == GAMEMODE_CUTSCENE) {
      image(bg_cutscene, bg_x, bg_y);
    } else {
      image(bg, bg_x, bg_y);
    }
  }
  popStyle();
  popMatrix();

  // ////////////////////////////////////////////////////////////
  // TREES
  for (int i = trees.size () - 1; i >= 0; i--) {
    Sprite e = trees.get(i);
    e.update();
    e.draw();
  }

  // ////////////////////////////////////////////////////////////
  // TREETOP LEAVES
  for (Sprite s : treetop_leaves) {
    s.update();
    s.draw();
  }

  // ////////////////////////////////////////////////////////////
  // RAM
  pushMatrix();
  pushStyle();
  ram.update();
  if (isRamHit) {
    tint(3, 50, 100);
    isRamHit = false;
  }
  ram.draw();
  popStyle();
  popMatrix();

  // ////////////////////////////////////////////////////////////
  // CLOUDS
  for (Sprite cloud : clouds) {
    pushMatrix();
    scale(0.25f);
    translate(cloud.pos.x * 3, cloud.pos.y * 3);
    cloud.draw();
    popMatrix();
  }
  
  // Draw FPS
//  drawPlayscreenText();
}

public void drawScoreScreen() {
  // GRAY OUT BACKGROUND, SLIGHTLY
  pushStyle();
  PImage curScreen = this.get();
  filter(GRAY);
  tint(100, 54);
  image(curScreen, 0, 0);
  popStyle();
  // ////////////////////////////////////////////////////////////
  // DRAW SCORESCREEN

  float overallProgress = mapWithCap(screenFrameCount, 0, 90, -0.2f, 1);

  pushMatrix();
  pushStyle();

  float alphaLevel = overallProgress * 100;

  tint(100, alphaLevel * 0.7f);

  translate(swidth / 2, sheight * 0.47f - 100);
  int w = gameOverPopup.width;
  int h = gameOverPopup.height;
  translate(-w / 2, -h / 2);

  // Draw Overlay Image
  float progressLvl = 0;

  //  progressLvl = mapWithCap(overallProgress, 0, 0.2f, 0, 1);
  //  tint(100, progressLvl * 100);
  //  image(gameOverPopup, 0, 0);

  // Text Attributes
  textFont(bebasFont, 36);
  textSize(36);
  textAlign(LEFT, CENTER);
  float barTextOffsetX = 90;
  float barTextOffsetY = 41;

  // DRAW ELEMENTS
  // gameOvertitle
  PImage img;
  img = gameOverGameOver;
  pushMatrix();
  translate(82, 214);
  translate(img.width / 2, img.height / 2);
  progressLvl = mapWithCap(overallProgress, 0.1f, 0.25f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-img.width / 2, -img.height / 2);
  // tint(100, 100);
  // fade in GameOver
  tint(100, progressLvl * 100);
  // fade out GameOver (for logo introduction)
  float progressLvl2 = mapWithCap(overallProgress, 0.8f, 0.9f, 0, 1);
  if (progressLvl2 > 0) {
    tint(100, (1 - progressLvl2) * 100);
  }
  image(img, 0, 0);
  popMatrix();

  // logo
  img = logo;
  pushMatrix();
  translate(174, 89);
  scale(0.4f);
  translate(img.width / 2, img.height / 2);
  progressLvl = mapWithCap(overallProgress, 0.85f, 0.95f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-img.width / 2, -img.height / 2);
  tint(100, progressLvl * 100);
  image(img, 0, 0);
  popMatrix();

  // gameOverCombo
  img = gameOverCombo;
  pushMatrix();
  translate(138, 360);
  translate(img.width / 2, img.height / 2);
  progressLvl = mapWithCap(overallProgress, 0.3f, 0.4f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-img.width / 2, -img.height / 2);
  tint(100, progressLvl * 100);
  image(img, 0, 0);
  // text
  fill(100, progressLvl * 100);
  text(stat_pbCombo, barTextOffsetX, barTextOffsetY);
  popMatrix();

  // gameOverBest1
  img = gameOverBest;
  pushMatrix();
  translate(401, 360);
  translate(img.width / 2, img.height / 2);
  progressLvl = mapWithCap(overallProgress, 0.35f, 0.45f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-img.width / 2, -img.height / 2);
  tint(100, progressLvl * 100);
  image(img, 0, 0);
  // text
  fill(100, progressLvl * 100);
  text(stat_gbCombo, barTextOffsetX, barTextOffsetY);
  popMatrix();

  // gameOverKills
  img = gameOverKills;
  pushMatrix();
  translate(138, 459);
  translate(img.width / 2, img.height / 2);
  progressLvl = mapWithCap(overallProgress, 0.4f, 0.5f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-img.width / 2, -img.height / 2);
  tint(100, progressLvl * 100);
  image(img, 0, 0);
  // text
  fill(100, progressLvl * 100);
  text(stat_pbKills, barTextOffsetX, barTextOffsetY);
  popMatrix();

  // gameOverBest2
  img = gameOverBest;
  pushMatrix();
  translate(401, 459);
  translate(img.width / 2, img.height / 2);
  progressLvl = mapWithCap(overallProgress, 0.45f, 0.55f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-img.width / 2, -img.height / 2);
  tint(100, progressLvl * 100);
  image(img, 0, 0);
  // text
  fill(100, progressLvl * 100);
  text(stat_gbKills, barTextOffsetX, barTextOffsetY);
  popMatrix();

  // gameOverTapReplay
  img = gameOverTapReplay;
  pushMatrix();
  pushStyle();
  translate(139, 579);
  translate(img.width / 2, img.height / 2);
  progressLvl = mapWithCap(overallProgress, 0.75f, 0.9f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-img.width / 2, -img.height / 2);
  float blinking = 100;
  if (floor(screenFrameCount / 15) % 2 == 0) {
    blinking = 0.0f;
  } else {
    blinking = 100f;
  }
  tint(100, progressLvl * blinking);
  image(img, 0, 0);
  popStyle();
  popMatrix();

  popStyle();
  popMatrix();

  // DRAW PLAY BUTTON
  // playBtn.draw(); //playBtn now unused.

  // Also draw playscreenText here for now
//  drawPlayscreenText();
}

public void drawCutscene() {
  sheep.draw();
  sheep.update();

  if (sheep.framecount >= 150) {
    ships.drawOriginalSize(ships.pos);
    ships.update();
  }

  if (sheep.framecount >= 239) {
    this.changeGameModeTo(GAMEMODE_STARTSCREEN);
  }
}

public void drawStartScreen() {
  // Time animations
  float overallProgress = mapWithCap(screenFrameCount, 0, 15, 0, 1);

  // Draw logo
  pushMatrix();
  pushStyle();
  translate(480, 0);
  scale(0.4f);
  translate(logo.width / 2, logo.height / 2);
  float progressLvl = mapWithCap(overallProgress, 0.85f, 1.0f, 0, 1);
  scale(elasticValueAtPctgProgress(progressLvl));
  translate(-logo.width / 2, -logo.height / 2);
  tint(100, progressLvl * 100);
  image(logo, 0, 0);
  popStyle();
  popMatrix();

  pushMatrix();
  pushStyle();
  translate(440, 500);
  float blinking = 100;
  if (floor(screenFrameCount / 15) % 2 == 0) {
    blinking = 0.0f;
  } else {
    blinking = 100f;
  }
  tint(100, blinking);
  image(gameOverTapReplay, 0, 0);
  popStyle();
  popMatrix();
}

public void drawPlayScreen() {
  pushMatrix();
  pushStyle();
  noStroke();

  // ////////////////////////////////////////////////////////////
  // CRATERS
  for (int i = craters.size () - 1; i >= 0; i--) {
    Sprite crater = craters.get(i);
    crater.update();
    crater.draw();
    if (crater.isFinished()) {
      craters.remove(i);
    }
  }

  // ////////////////////////////////////////////////////////////
  // DEAD BODIES
  for (int i = vikings.size () - 1; i >= 0; i--) {
    Squad squad = vikings.get(i);
    if (squad.isEmpty()) {
      vikings.remove(i);
      continue;
    }
    squad.update();
    squad.drawOnlyDead();
  }

  // ////////////////////////////////////////////////////////////
  // VIKINGS
  for (int i = vikings.size () - 1; i >= 0; i--) {
    Squad squad = vikings.get(i);
    if (squad.isEmpty()) {
      vikings.remove(i);
      continue;
    }
    squad.drawOnlyAlive();
  }

  // ////////////////////////////////////////////////////////////
  // EXPLOSIONS
  for (int i = explosions.size () - 1; i >= 0; i--) {
    Sprite e = explosions.get(i);
    e.update();
    e.draw();
    if (e.isFinished()) {
      explosions.remove(i);
    }
  }
  for (int i = flame_explosions.size () - 1; i >= 0; i--) {
    Sprite e = flame_explosions.get(i);
    e.update();
    e.draw();
    if (e.isFinished()) {
      flame_explosions.remove(i);
    }
  }

  // ////////////////////////////////////////////////////////////
  // BLOODSPLATS
  for (int i = bloodsplats.size () - 1; i >= 0; i--) {
    Sprite e = bloodsplats.get(i);
    e.update();
    e.draw();
    if (e.isFinished()) {
      bloodsplats.remove(i);
    }
  }

  // ////////////////////////////////////////////////////////////
  // COMBO TEXT DISPLAY
  // if (singleComboDisplay != null) {
  // if (singleComboDisplay.isAlive) {
  // singleComboDisplay.update();
  // singleComboDisplay.draw();
  // }
  // }
  for (int i = 0; i < comboDisplays.size (); i++) {
    ComboDisplay display = comboDisplays.get(i);
    if (display.isAlive) {
      display.update();
      display.draw();
    } else {
      // remove
      comboDisplays.remove(i);
      i--;
    }
  }

  // drawPlayscreenText();

  popStyle();
  popMatrix();
}

public void drawRegardlessForeground() {
  // ////////////////////////////////////////////////////////////
  // BUSHES
  pushMatrix();
  scale(1.5f);
  bush1.update();
  bush1.draw();
  bush2.update();
  bush2.draw();
  popMatrix();

  // ////////////////////////////////////////////////////////////
  // FOREGROUND
  pushMatrix();
  pushStyle();
  image(fg, fg_x, fg_y);
  fg_tree.update();
  fg_tree.draw();
  popStyle();
  popMatrix();

  // ////////////////////////////////////////////////////////////
  // ATMOSPHERE PARTICLES
  for (int i = leaves.size () - 1; i >= 0; i--) {
    Sprite e = leaves.get(i);
    e.update();
    e.pos.x -= random(10, 30);
    e.pos.y += random(5, 10);
    e.angle += random(0.001f, 0.1f);
    e.draw();
    if (e.pos.x < -50) {
      leaves.remove(i);
    }
  }
}

public void drawGUI() {
  // ////////////////////////////////////////////////////////////
  // HEALTHBAR (IN GOLD!) (Superhackish)
  goldbar_bg.update();
  goldbar.update();
  goldbar_frame.update();
  goldbar_bg.draw();
  PImage img = goldbar.getFrame();
  pushMatrix();
  translate(goldbar.pos.x - 119, goldbar.pos.y - 4);
  scale(life / initialLife * 0.75f, 0.75f);
  image(img, 0, 0);
  popMatrix();
  goldbar_frame.draw();
}

public void draw() {

  screenFrameCount++;

  updateRegardless();
  drawRegardless();
  if (gameMode.equals(GAMEMODE_PLAYSCREEN)) {
    drawPlayScreen();
  }
  if (gameMode.equals(GAMEMODE_CUTSCENE)) {
    drawCutscene();
  }
  if (gameMode.equals(GAMEMODE_STARTSCREEN)) {
    drawStartScreen();
  }
  drawRegardlessForeground();
  if (gameMode.equals(GAMEMODE_SCORESCREEN)) {
    drawScoreScreen();
  }
  if (gameMode.equals(GAMEMODE_PLAYSCREEN)) {
    drawGUI();
    if (life == 0) {
      this.changeGameModeTo(GAMEMODE_SCORESCREEN);
    }
  }
}

public void changeGameModeTo(String newGameMode) {
  // Make sure the scorescreen doesn't trigger twice
  if (newGameMode.equals(GAMEMODE_PLAYSCREEN)) { 
    resetVariablesForNewGame();
  }
  gameMode = newGameMode;
  screenFrameCount = 0;

  if (newGameMode.equals(GAMEMODE_SCORESCREEN)) {
    // pause gamePlay music
    electroHorn_audio.pause();
    // play gameScore music
    scoreScreen_audio.rewind();
    scoreScreen_audio.loop();

    // trigger gameOver sound
    game_over_sfx.trigger();

    // Update Stats
    if (stat_pbCombo > stat_gbCombo) {
      stat_gbCombo = stat_pbCombo;
    }
    if (stat_pbKills > stat_gbKills) {
      stat_gbKills = stat_pbKills;
    }
  } else if (newGameMode.equals(GAMEMODE_CUTSCENE)) {
    ships.rewind();
    sheep.rewind();
  } else if (newGameMode.equals(GAMEMODE_PLAYSCREEN)) {
    // pause gameScore music
    scoreScreen_audio.pause();

    // replay gamePlay music
    electroHorn_audio.rewind();
    electroHorn_audio.loop();

    // reset variables
    resetVariablesForNewGame();
  } else if (newGameMode.equals(GAMEMODE_STARTSCREEN)) {
    // Stuff?
  }
}

public void drawPlayscreenText() {
  // PRINT TEXT
  pushStyle();
  textFont(customFont, 36);

  String descript = "";
  descript += "FrameRate:   " + frameRate;
  descript += "\n";
  descript += "\n";
  descript += "\n";
  descript += "Kills:   " + stat_pbKills;
  descript += "\n";
  descript += "Combo:   " + stat_curCombo;
  descript += "\n";
  descript += "PB Combo:   " + stat_pbCombo;
  fill(0, 0, 100);
  textSize(18);
  textAlign(LEFT, TOP);
  text(descript, 10, 10);

  popStyle();
}

public void calculateCombos() {
  if (latestHitEvent == null)
    return;

  // Update curCombo
  stat_curCombo = stat_pbKills - prv_stat_pbKills;

  // Execute Combo related events
  float x = latestHitEvent.x;
  float y = latestHitEvent.y;
  if (stat_curCombo > stat_pbCombo) {
    stat_pbCombo = stat_curCombo;
  }
  if (stat_curCombo > minComboToDisplay) {
    // if (singleComboDisplay == null) {
    // singleComboDisplay = new ComboDisplay(stat_curCombo);
    // singleComboDisplay.pos = new PVector(x, y, 0);
    // }
    // if (!singleComboDisplay.isAlive) {
    // singleComboDisplay = new ComboDisplay(stat_curCombo);
    // singleComboDisplay.pos = new PVector(x, y, 0);
    // }
    // /////////////// multiple versions
    String a = (stat_curCombo + 0) + "";
    ComboDisplay comboDisplay = new ComboDisplay(a);
    comboDisplay.pos = new PVector(x, y, 0);
    comboDisplays.add(comboDisplay);
    println(stat_curCombo);
  }

  prv_stat_pbKills = stat_pbKills;
}

public class PlayButton {
  PVector location;
  PVector dimensions;

  PlayButton() {
    location = new PVector(20, 20);
    dimensions = new PVector(60, 30);
  }

  void draw() {
    // centre mode drawing
    pushMatrix();
    pushStyle();

    translate(location.x, location.y);
    translate(-dimensions.x / 2, -dimensions.y / 2);
    // Draw Rect
    strokeWeight(2);
    fill(30, 80, 40, 30);
    rect(0, 0, dimensions.x, dimensions.y);

    // Draw Text
    textFont(customFont, 36);
    textSize(24);
    fill(0, 0, 100);
    text("Play", 20, 60);

    popStyle();
    popMatrix();
  }

  boolean hitTest(float x, float y) {
    PVector corner1 = new PVector(location.x - dimensions.x / 2, location.y - dimensions.y / 2);
    PVector corner2 = new PVector(location.x + dimensions.x / 2, location.y + dimensions.y / 2);
    if ((x > corner1.x) && (x < corner2.x)) {
      if ((y > corner1.y) && (y < corner2.y)) {

        // if hit, also play its sound
        playBtn_sfx.trigger();

        return true;
      }
    }
    return false;
  }
}

public void hitEvent(float x, float y) {
  latestHitEvent = new PVector(x, y, 0);

  if (gameMode.equals(this.GAMEMODE_CUTSCENE)) {
    this.changeGameModeTo(GAMEMODE_STARTSCREEN);
    return;
  }
  if (gameMode.equals(this.GAMEMODE_STARTSCREEN)) {
    this.changeGameModeTo(GAMEMODE_PLAYSCREEN);
    return;
  }
  if (gameMode.equals(this.GAMEMODE_SCORESCREEN)) {
    // if (playBtn.hitTest(x, y)) {
    if (screenFrameCount > 90) { // so that you can't instantly press replay
      this.changeGameModeTo(GAMEMODE_PLAYSCREEN);
    }
    // explosion_sfx.trigger();
    // }
    return;
  }
  if (gameMode.equals(this.GAMEMODE_PLAYSCREEN)) {
    boolean explosionInSky = false;
    if (y < treasure_line - 30) {
      explosionInSky = true;
    }
    PVector hitPos = new PVector(x, y);
    this.addExplosion(hitPos);
    if (!explosionInSky) {
      this.addCrater(hitPos);
    }
    for (int i = vikings.size () - 1; i >= 0; i--) {
      Squad squad = vikings.get(i);
      squad.hit(hitPos, 80);
    }
    return;
  }
}

public void mousePressed() {
  this.hitEvent(mouseX, mouseY);
}

// custom test function - don't publish
public void keyPressed() {
  if (key == 'g') {
    if (gameMode.equals(this.GAMEMODE_SCORESCREEN)) {
      this.changeGameModeTo(GAMEMODE_PLAYSCREEN);
      return;
    }
    if (gameMode.equals(this.GAMEMODE_PLAYSCREEN)) {
      this.changeGameModeTo(GAMEMODE_SCORESCREEN);
      return;
    }
  }
  // Don't let Processing get you
  if (key == ESC) {
    key = 0;
  }
}

public void keyReleased() {
  if (key == ESC) {
    if (gameMode == GAMEMODE_CUTSCENE) {
      changeGameModeTo(GAMEMODE_STARTSCREEN); 
      return;
    } else if (gameMode == GAMEMODE_STARTSCREEN) {
      changeGameModeTo(GAMEMODE_PLAYSCREEN); 
      return;
    }
  } 
  // Temporary key to exit game - don't publish
  if (key == 'q') {
    exit();
  }
  // Temporary key to toggle difficulty - don't publish
  if (key == 'r') {
    if (spawn_rate == 6) {
      spawn_rate = 12;
      base_speed = 4;
    } else {
      spawn_rate = 6;
      base_speed = 2;
    }
  }
  // Temporary key to lose game - don't publish
  if (key == 'L') {
    life = 0;
  }
}

public Sprite addExplosion(PVector pos) {
  Sprite e = new Sprite("explosion", "FX/Explosion_Small/Explosion_small%d.png", 1, 27, 4);
  e.setPos(new PVector(pos.x, pos.y));
  explosions.add(e);
  is_explosion_queued = true;
  return e;
}

public Sprite addFlameExplosion(PVector pos) {
  Sprite e = new Sprite("flame_explosion", "FX/Explosion_Bomba/ExplosionBombaMan%d.png", 1, 15, 4);
  e.setPos(new PVector(pos.x, pos.y));
  flame_explosions.add(e);
  return e;
}

public Sprite addCrater(PVector pos) {
  Sprite e = new Sprite("crater", "FX/ExplosionHit.png");
  e.setPos(new PVector(pos.x, pos.y));
  e.setLife(100);
  craters.add(e);
  return e;
}

public Sprite addBloodsplat(PVector pos) {
  Sprite e = new Sprite("bloodsplat", "FX/BloodSplat/BloodSplat%d.png", 1, 9, 4);
  e.setPos(new PVector(pos.x, pos.y));
  bloodsplats.add(e);
  return e;
}

public Sprite addTree(PVector pos) {
  Sprite e = new Sprite("tree", "FX/Tree/TreePushed%d.png", 1, 7, 4);
  e.setPos(new PVector(pos.x, pos.y));
  e.framecount = 6;
  trees.add(e);
  return e;
}

public void addTreetopLeaves() {
  Sprite t1 = new Sprite("treetop_leaves_1", "FX/Treetop/Treetop1%d.png", 1, 21, 4);
  t1.setPos(new PVector(825, 100));
  t1.framecount = 20;
  treetop_leaves.add(t1);
  Sprite t2 = new Sprite("treetop_leaves_2", "FX/Treetop/treeTop2%d.png", 1, 40, 4);
  t2.setPos(new PVector(775, 30));
  t2.framecount = 39;
  treetop_leaves.add(t2);
  Sprite t3 = new Sprite("treetop_leaves_3", "FX/Treetop/Treetop3%d.png", 1, 10, 4);
  t3.setPos(new PVector(300, 25));
  t3.framecount = 9;
  treetop_leaves.add(t3);
  Sprite t4 = new Sprite("treetop_leaves_2", "FX/Treetop/treeTop4%d.png", 1, 20, 4);
  t4.setPos(new PVector(50, 25));
  t4.framecount = 19;
  treetop_leaves.add(t4);
}

public void addClouds() {
  for (int i = 1; i <= 3; i++) {
    Sprite s = new Sprite("cloud" + i, "FX/Cloud" + i + ".png");
    s.setPos(new PVector(random(width), random(50, 150)));
    clouds.add(s);
  }
}

public void addBushes() {
  bush1 = new Sprite("bush1", "FX/Bushes/Bush1%d.png", 1, 7, 4);
  bush1.setPos(new PVector(460, 420));
  bush1.framecount = 6;

  bush2 = new Sprite("bush2", "FX/Bushes/Bush2%d.png", 1, 7, 4);
  bush2.setPos(new PVector(330, 420));
  bush2.framecount = 6;
}

public void ramDamage(float damage) {
  life -= damage;
  if (life >= 100) {
    ram.framecount = 4;
  } else if (life < 100 && life >= 75) {
    ram.framecount = 3;
  } else if (life >= 50) {
    ram.framecount = 2;
  } else if (life >= 25) {
    ram.framecount = 1;
  } else {
    ram.framecount = 0;
  }
  if (life < 0) {
    life = 0;
  }

  isRamHit = true;

  // Trigger a random pain sfx
  //  if (random(1.0f) < 0.5f) {
  //    painOw_sfx.trigger();
  //  } else {
  //    painStolen_sfx.trigger();
  //  }
}

public void addScreenShake(int frames) {
  screenshake_frames = frames;
  screenshakeX = random(1) * abs(randomGaussian() * 5 + 5);
  screenshakeY = random(1) * abs(randomGaussian() * 10 + 10);
}

public void makeScreenShake() {
  if (screenshake_frames <= 0) {
    return;
  }
  screenshake_frames--;
  screenshakeX *= -0.6;
  screenshakeY *= -0.6;
  translate(screenshakeX, screenshakeY);
}
