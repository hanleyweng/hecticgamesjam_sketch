// ----------------------------------------------------
// COMMUNICATION TAB

// Vars to prevent OSC Overload
int oscKpos_lastFrameCount = 0;
int oscKpos_curHitsInFrame = 0;
int oscKpos_maxHitsPerFrame = 3;


public void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
//  print("### received an osc message.");
//  print(" addrpattern: " + theOscMessage.addrPattern());
//  println(" typetag: " + theOscMessage.typetag());

  // Handle Kinect HitPoint message
  if (kinectInputAllowed) {
    if (theOscMessage.addrPattern().equals("/kpos")) {

      // reset allowed hits every frame
      if (oscKpos_lastFrameCount != frameCount) {
        oscKpos_curHitsInFrame = 0; 
        oscKpos_lastFrameCount = frameCount;
      }

      // allow a hit only if the maximum hasnt been reached
      if (oscKpos_curHitsInFrame <= oscKpos_maxHitsPerFrame) {
        // Note - have not tested if any OSC messages are lost / confused between x and y
        float posX = theOscMessage.get(0).floatValue();
        float posY = theOscMessage.get(1).floatValue();
        this.hitEvent(posX * swidth, posY * sheight);

        // Increment the number of hits occured in this frame
        oscKpos_curHitsInFrame++;
      }
    }
  }
}

