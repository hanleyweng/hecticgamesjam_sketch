// ----------------------------------------------------
// SQUAD TAB
int miniboss_counter = 0;

public class Squad {
  ArrayList<Viking> squad;
  float hitbox = 5;
  float speed = min(base_speed + screenFrameCount * 0.0001f, 5);
  float confidence = 0.5f;
  int scared_threshold = 1;

  Squad(PVector pos, int number, float spreadX, float spreadY) {
    squad = new ArrayList<Viking>();

    for (int i = 0; i < number; i++) {
      float r = random(1);
      Viking viking;
      
      miniboss_counter++;
      if (miniboss_counter % 16 == 0) {
        viking = new Viking("miniboss"); 
      } else if (r < 0.33) {
        viking = new Viking("main");
      } else if (r < 0.66) {
        viking = new Viking("alt");
      } else if (r < 0.75) {
        viking = new Viking("bomba");
      } else if (r < 0.90) {
        viking = new Viking("horse");
      } else {
        viking = new Viking("flag");
      }
      PVector displacement = new PVector();
      if (i > 0) {
        displacement.add(new PVector(abs(randomGaussian() * spreadX), randomGaussian() * spreadY));
      }

      PVector _pos = new PVector();
      _pos.add(pos);
      _pos.add(displacement);
      viking.setPos(_pos);
      squad.add(viking);
    }

    Collections.sort(squad, new SpriteDepthComparitor());
  }

  public void hit(PVector target, float radius) {
    int alive = squad.size();
    for (int i = alive - 1; i >= 0; i--) {
      Viking viking = squad.get(i);
      if (viking.pos.dist(target) - hitbox <= radius) {
        //

        alive--;
        viking.hit(target);
      } else if (viking.pos.dist(target) - hitbox <= (radius + knockback_radius)) { // Shockwave
        viking.knockback(target);
      }
    }

    // Vikings get scared in sudden low numbers
    if (alive <= scared_threshold) {
      for (int i = squad.size () - 1; i >= 0; i--) {
        Viking viking = squad.get(i);
        if (viking.state == "run" && (viking.type == "main" || viking.type == "alt")) {
          viking.state = "run_scared";
        }
      }
    }
  }

  public void add(Viking v) {
    this.squad.add(v);
  }

  public Viking get(int i) {
    return this.squad.get(i);
  }

  public void remove(int i) {
    this.squad.remove(i);
  }

  public void update() {
    for (int i = squad.size () - 1; i >= 0; i--) {
      Viking viking = squad.get(i);
      viking.update();
      viking.advance(speed, confidence);
      if ((viking.state == "hit" && viking.isFinished()) || viking.pos.x < -50 || viking.pos.x > (width * 2)) {
        squad.remove(i);
      }
    }
  }

  public void draw() {
    for (Viking viking : squad) {
      viking.draw();
    }
  }

  public void drawOnlyAlive() {
    for (Viking viking : squad) {
      if (viking.state != "hit") {
        viking.draw();
      }
    }
  }

  public void drawOnlyDead() {
    for (Viking viking : squad) {
      if (viking.state == "hit") {
        viking.draw();
      }
    }
  }

  public boolean isEmpty() {
    return squad.size() == 0;
  }
}

class SpriteDepthComparitor implements Comparator<Viking> {
  @Override
    public int compare(Viking a, Viking b) {
    if (a.pos.y == b.pos.y) {
      return 0;
    } else {
      return a.pos.y < b.pos.y ? -1 : 1;
    }
  }
}

